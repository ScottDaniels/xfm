
# given two files, compare their md5 and exit good if they match

m1=$( cat $1 | md5sum ) 
m2=$( cat $2 | md5sum ) 

if [[ $m1 != $m2 ]]
then
	echo "[FAIL] $1 does not match $2" .&2
	exit 1
fi

exit 0
