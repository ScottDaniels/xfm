
#	Mnemonic:	fail_if.ksh
#	Abstract:	Executes the command given on the command line, and exits
#				with a failure if the command is successful. If the first token
#				of the command is a bang (!), then fails if the command fails.
#				This mainly allows us to grep for a pattern and faial when it's 
#				found, but might have other uses making it easy to fail from 
#				a make file when grep would normally returns good.
#
#	Date:		10 July 2020
#	Author:		E. Scott Daniels
# --------------------------------------------------------------------------

not="!"
if [[ $1 == "!"* ]]
then
	not=""
	shift
fi

# do not put any commands between these two!
"$@"
rc=$?
exit $(( $not $rc ))

