
MKSHELL=ksh
< ../examples/master.mk

# specifically run against the binaries built in the source and not what is installed
PATH=../.build/src/pfm:../.build/src/hfm:../.build/src/tfm:$PATH 

tfm = ../.build/src/tfm/tfm
pfm = ../.build/src/pfm/pfm
hfm = ../.build/src/hfm/hfm
XFM_PATH = .:../templates/

all:V: test_temp_indent test_pgd_col.ps  test_issue2 test_hanno test_imbed \
	test_eval test_if test_ca test_flush test_gv test_cmd test_issue8 test_nlesc\
	test_issue11 strike_test pageCount_test

test_temp_indent:Q: test_temp_indent.xfm
	echo "temp indent test"
	pfm -G 4.5x6	${prereq} ${target}.ps >test_temp_indent.log 2>&1
	tfm ${prereq} ${target}.txt >>test_temp_indent.log 2>&1
	validate_m5.ksh ${target}.txt ${target}.expect

# test issue 10 -- double space
test_ds:Q:
	echo "test_ds"
	TFM_PATH=.:../templates/ XFM_OUTPUT_TYPE=rst tfm test_ds.xfm  test_ds.rst
	TFM_PATH=.:../templates/ XFM_OUTPUT_TYPE=md tfm test_ds.xfm  test_ds.md
	# no automated verification for this at this point

# small doc with banner; manual verification only. Pass 1 page number should just be the
# number. Pass two should have 'n of t' format for page number.
test_banner:Q:
	rm -f _fref.ca
	$pfm banner.xfm banner_p1.ps
	$pfm banner.xfm banner_p2.ps

# test as is commands; may require some manual verification
test_ai:Q:
	pfm test_ai.xfm test_ai.ps
	hfm test_ai.xfm test_ai.html
	fail_if.ksh grep "&amp;" test_ai.html
	fail_if.ksh grep "\\[(]" test_ai.ps
	# expect lines with variable subs should be at leading edge, so this catches unexpanded vars
	fail_if.ksh grep "^expect that &climbing" test_ai.ps
	fail_if.ksh grep "^expect that &climbing" test_ai.html

# requires manual verification
pageCount_test:Q:
	echo "page count test"
	pfm test_pgcount.xfm test_pgcount.ps

# requires manual verification
strike_test:Q:
	echo "strike test"
	pfm strike_test.xfm strike_test.ps

# requires manual verification
test_issue11:Q:
	echo "issue 11 test"
	pfm test_issue11.xfm test_issue11.ps

# requires manual verification
test_cbreak:Q:
	echo "cbreak test"
	pfm test_cbreak.xfm test_cbreak.ps
	tfm test_cbreak.xfm test_cbreak.txt
	hfm test_cbreak.xfm test_cbreak.html

# test issue 15 -- Variable font size for constant width text
test_cwfs:Q:
	echo "test_cwfs"
	XFM_PATH=.:../templates/ XFM_OUTPUT_TYPE=ps pfm test_fontsz.xfm  test_fontsz.ps
	# no automated verification for this at this point
	
# test output from the cmd_* macros
test_cmd:Q:
	echo "test_cmd (mark down)"
	TFM_PATH=.:../templates/:/usr/local/share/xfm XFM_OUTPUT_TYPE=md tfm test_cmd.xfm  test_cmd.md 2>/dev/null
	echo "test_cmd (rst)"
	TFM_PATH=.:../templates/:/usr/local/share/xfm XFM_OUTPUT_TYPE=rst tfm test_cmd.xfm  test_cmd.rst 2>/dev/null
	fail_if.ksh grep  " \*\*.* .*\*\*" test_cmd.rst
	TFM_PATH=.:../templates/:/usr/local/share/xfm pfm test_cmd.xfm  test_cmd.ps 2>/dev/null

# test ability to escape newlines in macro parameter list
test_nlesc:Q: test_nlesc.xfm
	echo "test_nlesc"
	tfm test_nlesc.xfm test_nlesc.txt 2>/dev/null
	validate_m5.ksh ${target}.txt ${target}.expect

# test after major tfm flush revamp in 2020
test_flush:Q: test_flush.xfm
	echo "test_flush"
	tfm test_flush.xfm test_flush.txt 2>/dev/null
	validate_m5.ksh ${target}.txt ${target}.expect

test_pgd_col.ps:Q: test_pgd_col.xfm
	echo "pgd column test"
	pfm -G 9.25x6	${prereq} ${target}
	echo "validation requires visual inspection of ${target}"

# output will contain "FAILED" if any tests fail, so just serarch the output
test_eval:Q:
	echo "evaluation test"
	tfm test_eval.xfm test_eval.txt 2>/dev/null
	fail_if.ksh grep  FAIL test_eval.txt

# output will contain "FAILED" if any tests fail, so just serarch the output
test_gv:Q:
	echo "get value test"
	tfm test_gv.xfm test_gv.txt 2>/dev/null
	fail_if.ksh grep  FAIL test_gv.txt

# output will contain "FAILED" if any tests fail, so just serarch the output
test_if:Q:
	echo "integer if test"
	tfm test_if.xfm test_if.txt 2>/dev/null
	fail_if.ksh grep FAIL test_if.txt

# output will contain "FAILED" if any tests fail, so just serarch the output
test_dif:Q:
	echo "double if test"
	tfm test_if.xfm test_dif.txt 2>/dev/null
	fail_if.ksh grep FAIL test_dif.txt

test_imbed:Q:
	echo "imbed test"
	pfm test_imbed.xfm ${target}.ps >test_imbed.log 2>&1
	tfm test_imbed.xfm ${target}.txt >>test_imbed.log 2>&1
	validate_m5.ksh ${target}.txt ${target}.expect

test_semver:Q:
	echo "sem ver test"
	tfm test_semver.xfm stdout

test_hanno:Q:
	echo "header annotation test"
	tfm test_hanno.xfm ${target}.rst >test_hanno.log 2>&1
	validate_m5.ksh ${target}.rst ${target}.expect

# output should not have any "indented" lines; fail if it does
test_ca:Q:
	echo "capture shift test"
	tfm test_ca.xfm /dev/null 2>/dev/null
	fail_if.ksh  grep -q '^ ' ca_test.ca

test_issue2:Q:
	echo "issue 2 test"
	tfm test_issue2.xfm test_issue2.txt >test_issue2.log 2>&1
	validate_m5.ksh test_issue2.txt issue2.expect 

# this bug caused a core dump, so a good return plus finding "capture" in the output means success
test_issue8:Q:
	echo "issue 8 test"
	XFM_PATH=.:../templates $tfm test_issue8.xfm test_issue8.txt 2>/dev/null
	fail_if.ksh ! grep -q capture test_issue8.txt	

test_mg:Q:
	echo "testing mg command"
	tfm test_mg.xfm stdout
	#validate_m5.ksh test_issue2.txt issue2.expect 

# requires manual, visual, verification
test_table:
	XFM_PATH=.:../templates $pfm test_table.xfm test_table.ps 2>/dev/null
	XFM_PATH=.:../templates $hfm test_table.xfm test_table.html 2>/dev/null

version:Q:
	tfm /dev/null stdout  2>&1|grep -i started
	pfm /dev/null stdout 2>&1|grep -i started
	hfm /dev/null stdout 2>&1|grep -i started

alphaHead:Q:
	XFM_PASS=1 pfm test_ahead.xfm /dev/null
	XFM_PASS=2 pfm test_ahead.xfm /tmp/alphahead.ps
	#tfm test_ahead.xfm /tmp/alphahead.txt
	#hfm test_ahead.xfm /tmp/alphahead.html

verify::
	echo "$PATH"
	which pfm
	which hfm
	which tfm

# ditch anything that can be rebuilt
nuke:Q:
	rm -f *.log *.ps *.txt *.rst *.ca
