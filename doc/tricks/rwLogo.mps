/rwl_a2 { arcto 4 { pop } repeat } def 
/rwl_p { gsave translate rotate 0 0 moveto show grestore } bind def
/rwl_boxWidth 70 def
/rwl_boxHeight 48 def
/rwl_oRadius  8 def

/paintLogo {
	gsave
	0 .5 .3 setrgbcolor		% background rounded corner box
	%84 83 translate
	rwl_boxWidth 2 div 0 moveto
	rwl_boxWidth 0 rwl_boxWidth rwl_boxHeight rwl_oRadius rwl_a2
	rwl_boxWidth rwl_boxHeight  0 rwl_boxHeight rwl_oRadius rwl_a2
	0 rwl_boxHeight 0 0  rwl_oRadius rwl_a2
	0 0 20 0 rwl_oRadius rwl_a2
	closepath
	fill
	grestore

	1 1 1 setrgbcolor
	gsave					% arc of text
	rwl_boxWidth 2 div rwl_boxHeight 2.8 div translate
	(Helvetica) findfont 10 scalefont setfont
	(R) 80.0 -22 3 rwl_p
	(o) 58.3 -19 12 rwl_p
	(u) 36.5 -13 18 rwl_p
	(x) 14.8 -5 22 rwl_p
	(W) -7.0 2 22 rwl_p
	(a) -31.8 12 19 rwl_p
	(r) -53.5 18 13 rwl_p
	(e) -66.0 21 9 rwl_p
	grestore

	(Helvetica) findfont 8 scalefont setfont	% text below
	8 6 moveto
	(A Software Mix) show

	(Helvetica) findfont 18 scalefont setfont	% initials
	21 15 moveto
	(RW) show
} bind def
