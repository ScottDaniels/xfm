# global mk patterns
# all {X}fm processing is done with two passes -- harmless if not needed.

MKSHEL = ksh

FONTS="-sFONTPATH=/usr/local/share/ghostscript/fonts -dSUBSTITUTEFONTS=true"
GS_FONTS="/usr/local/share/ghostscript/fonts"

%.md: %.xfm
	rm -f *.ca
	XFM_OUTPUT_TYPE=md XFM_PASS=1 tfm ${prereq%% *} /dev/null
	XFM_OUTPUT_TYPE=md XFM_PASS=2 tfm ${prereq%% *} $target

# TFM puts out a manditory lead space on each line. Sadly, as to be expected with anything
# python oriented, RST cannot deal with this, so we must chop it on output.
#
%.rst: %.xfm
	rm -f *.ca
	XFM_OUTPUT_TYPE=rst XFM_PASS=1 tfm ${prereq%% *} /dev/null
	XFM_OUTPUT_TYPE=rst XFM_PASS=2 tfm ${prereq%% *} |  sed 's/^ //' >$target 

%.txt: %xfm
	rm -f *.ca
	XFM_PASS=1 tfm ${prereq%% *} /dev/null
	XFM_PASS=2 tfm ${prereq%% *} $target

%.html: %.xfm
	rm -f *.ca
	XFM_PASS=1 hfm ${prereq%% *} /dev/null
	XFM_PASS=2 hfm ${prereq%% *} $target
	
%.txt: %.xfm
	rm -f *.ca
	XFM_PASS=1 tfm ${prereq%% *} /dev/null
	XFM_PASS=2 tfm ${prereq%% *} $target

%.ps: %.xfm
	rm -f *.ca
	XFM_PASS=1 pfm ${prereq%% *} /dev/null
	export XFM_FINGERPRINT=$( git log -n 1| awk ' /^commit/ {print substr( $2, 1,10 ) }' )
	XFM_PASS=2 pfm ${prereq%% *} $target

# rfm is deprecated, but we'll keep this
%.rtf: %.xfm
	rm -f *.ca
	XFM_PASS=1 rfm ${prereq%% *} /dev/null
	XFM_PASS=2 rfm ${prereq%% *} $target
	
# requires ghostscript
%.pdf::	%.ps
	gs $FONTS -dPDFX -dBATCH  -dNOPROMPT -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=${prereq%%.ps*}.pdf ${prereq%% *}

all:V: $ALL

nuke::
	rm -f $NUKE
