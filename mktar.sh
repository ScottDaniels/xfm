
#	Abstract:	This builds a tar file that can be extracted into someplace like
#				/usr/local to "install" {X}fm.  This is for environments that do
#				not support rpm or deb files (looking at pinetab2). The default
#				source is .build_arm.
#
#	Date:		12 June 2023
#	Author:		E. Scott Daniels
# ---------------------------------------------------------------------------------

sDir=${1:-.build_arm}
if [[ ! -d  $sDir ]]
then
	echo "cannot find source: $sDir"
	exit 1
fi

pbin=$sDir/src/pfm/pfm
hbin=$sDir/src/hfm/hfm
tbin=$sDir/src/tfm/tfm
imdir=templates

wrkDir=/tmp/PID$$.d
mkdir $wrkDir
mkdir $wrkDir/bin
mkdir -p $wrkDir/share/xfm

id=$( date "+%Y%m%d" )

cp $pbin $hbin $tbin $wrkDir/bin/
cp $imdir/*.im $wrkDir/share/xfm/

(
	cd $wrkDir
	tar -cf - .| gzip >/tmp/xfm_$id.tgz
)

rm -fr $wrkDir
