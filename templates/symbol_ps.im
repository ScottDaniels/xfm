
.if false
	Mnemonic:	symbol_ps.im
	Abstract:	These are postscript definitions for various bits of the symbol
				font. If used in figures (.fg) they must be escaped:
					.fg Histogram with ^&PSI set to 200

				Greek letters have a lead underbar to prevent issues in the HTML
				world. That's not needed for other output, but to be consistent
				across all cmd_* imbed it must be here too.

				Only a subset of symbols are defined; the user is certainly free to 
				add to the list via their own set of definitions.

	Date:		18 September 2021
	Author: 	E. Scott Daniels
.fi

.if ! __sym_ps_im
.dv __sym_ps_im 1

.** Greek alphabets upper and lower
.dv _ALPHA .tf Symbol ^&text_size A ^:
.dv _BETA .tf Symbol ^&text_size B ^:
.dv _CHI .tf Symbol ^&text_size C ^:
.dv _DELTA .tf Symbol ^&text_size D ^:
.dv _EPSILON .tf Symbol ^&text_size E ^:
.dv _PHI .tf Symbol ^&text_size F ^:
.dv _GAMMA .tf Symbol ^&text_size G ^:
.dv _ETA  .tf Symbol ^&text_size H ^:
.dv _IOTA  .tf Symbol ^&text_size I ^:
.dv _KAPPA  .tf Symbol ^&text_size K ^:
.dv _LAMBDA  .tf Symbol ^&text_size L ^:
.dv _MU .tf Symbol ^&text_size M ^:
.dv _NU .tf Symbol ^&text_size N ^:
.dv _XI .tf Symbol ^&text_size X ^:
.dv _OMICRON .tf Symbol ^&text_size O ^:
.dv _PI .tf Symbol ^&text_size P ^:
.dv _THETA  .tf Symbol ^&text_size Q ^:
.dv _RHO .tf Symbol ^&text_size R ^:
.dv _SIGMA .tf Symbol ^&text_size S ^:
.dv _TAU .tf Symbol ^&text_size T ^:
.dv _UPSILON .tf Symbol ^&text_size U ^:
.dv _OMEGA .tf Symbol ^&text_size W ^:
.dv _PSI .tf Symbol ^&text_size Y ^:
.dv _ZETA  .tf Symbol ^&text_size Z ^:

.dv _alpha .tf Symbol ^&text_size a ^:
.dv _beta .tf Symbol ^&text_size b ^:
.dv _chi .tf Symbol ^&text_size c ^:
.dv _delta .tf Symbol ^&text_size d ^:
.dv _epsilon .tf Symbol ^&text_size e ^:
.dv _phi .tf Symbol ^&text_size f ^:
.dv _gamma .tf Symbol ^&text_size g ^:
.dv _eta  .tf Symbol ^&text_size h ^:
.dv _iota  .tf Symbol ^&text_size i ^:
.dv _kappa  .tf Symbol ^&text_size k ^:
.dv _lambda  .tf Symbol ^&text_size l ^:
.dv _mu .tf Symbol ^&text_size m ^:
.dv _nu .tf Symbol ^&text_size n ^:
.dv _xi .tf Symbol ^&text_size x ^:
.dv _omicron .tf Symbol ^&text_size o ^:
.dv _pi .tf Symbol ^&text_size p ^:
.dv _theta  .tf Symbol ^&text_size q ^:
.dv _rho .tf Symbol ^&text_size r ^:
.dv _sigma .tf Symbol ^&text_size s ^:
.dv _tau .tf Symbol ^&text_size t ^:
.dv _upsilon .tf Symbol ^&text_size u ^:
.dv _omega .tf Symbol ^&text_size w ^:
.dv _psi .tf Symbol ^&text_size y ^:
.dv _zeta  .tf Symbol ^&text_size z ^:

.** symbols; \xxx pass all the way into the postscript and thus must be octal
.** 0xb0
.dv DEGREE .tf Symbol ^&text_size ^^^\260 ^:
.dv PLUS_MINUS .tf Symbol ^&text_size ^^^\261 ^:
.dv GEQUAL .tf Symbol ^&text_size ^^^\263 ^:
.dv DOT_PROD .tf Symbol ^&text_size ^^^\267 ^:
.dv NOT_EQUAL .tf Symbol ^&text_size ^^^\271 ^:
.dv THREE_EQUAL .tf Symbol ^&text_size ^^^\272 ^:
.dv APPROX_EQUAL .tf Symbol ^&text_size ^^^\273 ^:
.dv ELIPIS .tf Symbol ^&text_size ^^^\274 ^:
.dv EXISTS .tf Symbol ^&text_size ^^^\044 ^:
.dv NOT_EXISTS ! .sm .tf Symbol ^&text_size ^^^\044 ^:
.dv INTERSECT .tf Symbol ^&text_size ^^^\307 ^:

.** 0xc0
.dv NIL .tf Symbol ^&text_size ^^^\306 ^:
.dv INTSECT .tf Symbol ^&text_size ^^^\307 ^:
.dv UNION .tf Symbol ^&text_size ^^^\310 ^:
.dv SUBSET .tf Symbol ^&text_size ^^^\311 ^:
.dv SUBSET_OR_EQ .tf Symbol ^&text_size ^^^\312 ^:
.dv SUBSET_OR_EQ .tf Symbol ^&text_size ^^^\315 ^:
.dv ELEMENT .tf Symbol ^&text_size ^^^\316 ^:
.dv NOT_ELEMENT .tf Symbol ^&text_size ^^^\317 ^:
.dv ANGLE .tf Symbol ^&text_size ^^^\320 ^:

.** 0xdo
.dv REGISTERED  .tf Symbol ^&text_size ^^^\322 ^:
.dv COPYRIGHT  .tf Symbol ^&text_size ^^^\323 ^:
.dv TRADE_MARK  .tf Symbol ^&text_size ^^^\324 ^:
.dv SQ_ROOT  .tf Symbol ^&text_size ^^^\326 ^:
.dv NOT .tf Symbol ^&text_size ^^^\330 ^:
.dv AND .tf Symbol ^&text_size ^^^\331 ^:
.dv WEDGE .tf Symbol ^&text_size ^^^\331 ^:
.dv OR .tf Symbol ^&text_size ^^^\332 ^:
.dv DEC_WEDGE .tf Symbol ^&text_size ^^^\332 ^:
.dv XOR .tf Symbol ^&text_size ^^^\305 ^:


.dv EURO .tf Symbol ^&text_size ^^^\240 ^:
.dv LEQUAL .tf Symbol ^&text_size ^^^\243 ^:
.dv INFINITY .tf Symbol ^&text_size ^^^\245 ^:
.dv LE_ARROW .tf Symbol ^&text_size ^^^\254 ^:
.dv UP_ARROW .tf Symbol ^&text_size ^^^\255 ^:
.dv RT_ARROW .tf Symbol ^&text_size ^^^\256 ^:
.dv DN_ARROW .tf Symbol ^&text_size ^^^\257 ^:


.fi
