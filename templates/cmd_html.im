
.if fslse
	Mnemonic:	cmd_htmkl.im
	Abstract:	This is the common macro definitions file that should
				be used when generating HTML output with hfm. The macros here
				are the same as those in the other cmd_* files and a generic,
				single, imbed command can be used to pull in the right set
				depending on the formatter being used, and (in the case of
				text, markdown and RST) type defined:
					.im cmd_master.im

	Date:		19 September 2021
	Author:		E. Scott Daniels
	
.fi

.in 0i 
.ju off

.** ----------------- figure and table labels -----------------
.dv fig .fg $1
.dv fig_cen .fg x=center $1
.dv fig_table .fg t=table $1
.dv fig_table_cen .fg x=center t=table $1

.** ------- tables ----------------------------------------
.dv beg_table .ta $1 ^:
.dv beg_table_nb .ta b=0 $1 ^:
.dv end_table .et
.dv col .cl $1 ^:
.dv row .tr $1 ^:
.dv table_head 

.** ----- alternating colour table rows ------------------------------
.** user can define row colours, and group size, else we default
.** group size is the number of rows to group into a single colour LESS 1
.** (set to 3 to group 2 lines)
.**
.dv acColour0 &{acColour0!#e0ffe0}
.dv acColour1 &{acColour1!#80c080}
.dv _acCIdx 0
.dv _acGSize &{acGSize!3}
.dv _acGIdx &_acGSize
.dv acGSize .dv _acGSize ^[ $1 1 - ] ^: .dv _acGIdx ^[ $1 1 - ] ^:
.dv acRow .ev ^`.tr $1 l=0 c=^^^&acColour^&_acCIdx ^:^` ~
	.if ^&_acGIdx 0 = ^: ~
		.dv _acGIdx ^&_acGSize ^: ~
		.dv _acCIdx ^[ %.0f ^&_acCIdx 1 + 2 % ] ^: ~
	.ei ~
		.dv _acGIdx ^[ %.0f ^&_acGIdx 1 - ] ^: ~
	.fi

.** ------ positioning -------------------------------------
.dv center .ce $1
.dv beg_center .bc start
.dv end_center .bc end

.** deprecated
.dv center_start .bc start
.dv center_end .bc end



.** ------- break, space, escape ------------------------------
.dv esc $1  				.** in some mark* langagues wowrds must be escaped
.dv break  .br
.dv space .sp 1
.dv half_space .sp 1
.dv line_len .ll $1
.dv mult_space .sp $1
.dv line .br .ln
.dv fo .fo on
.dv nf .nf



.** ------------- images -----------------------------------------
.** image has different parms based on formatter and output
.** postscript  uses  $1, html uses $2 (url) and markdown uses $2 and 
.** $3 (url/caption)
.dv image  ^^<img src="$2"^^ \>

.** -----------  lists -------------------------------------------
.** dlist parms are space : all other .bd params : rst parms
.dv beg_dlist  .bd $1 $2
.dv end_dlist .ed
.dv di       .di $1 ^:
.dv di_space &space  .di $1 ^:
.dv adi .di ^:

.dv beg_list .bl $1
.dv end_list .el
.dv item .li
.dv li .li
.dv li_space &space .li 

.** list item characters
.dv lic1 *
.dv lic2 -
.dv lic3 +

.** --------------- indention --------------------------------------
.** leading indention is significant to markdown, so turn it off,
.** though tfm will always add a leading space which must be stripped.
.in 0
.dv indent  .bl
.dv uindent .el
.dv smindent .bl
.dv smuindent .el

.** --------------- fonts and headers --------------------------------
.dv cw ^<tt^>$1^</tt^>

.dv bold  ^<b^>$1^</b^>
.dv ital ^<i^>$1^</i^>

.dv colour .co $1
.dv big .st 20 $1 .st ^&textsize
.dv small .st 4 $1 .st ^&textsize
.dv colour ^<font color=$1^>$2^</font^>
.dv super .sm ^<sup^>$1^</sup^>

.dv set_font_bold .sf ^&bold_font
.dv set_font_ital .sf ^&ital_font
.dv set_font_normal .sf ^&text_font
.dv set_font_cw .sf ^&cw_font

.dv just .ju ${1!on}
.dv head_num .hn ${1!on}
.dv pg_num

.dv h1 .h1 $1
.dv h2 .h2 $1
.dv h3 .h3 $1
.dv h4 .h4 $1

.** ----------- column support ----------------------------------------
.dv _end_col 
.dv one_col ^&_end_col .dv _end_col ^^^^</div^^^^> ^: ^^<div style="width: &{html_col_width!640px}" ^^>
.dv two_col ^&_end_col .dv _end_col ^^^^</div^^^^> ^: ^^<div style="width: &{html_col_width!640px}" ^^>
.dv cleanup .ca vars _fref.ca ^&_end_col  ^: .cn showend .dv _no_banner 1 ^:

.** ------------- examples ---------------------------------
.dv beg_ex ^&half_space  .st ^&{ex_size!8p} .sf ^&cw_font .nf
.dv beg_ex_cen  ^&half_space  .st ^&{ex_size!8p} .sf ^&cw_font  .bc start .nf

.dv end_ex .fo on
.dv end_ex_fig .fo on .st ^&text_size .sf ^&text_font .fg $1
.dv end_ex_cfig .fo on .st ^&text_size .sf ^&text_font .fg x=center$1

.dv end_ex__cen_fig .fo on .bc end .st ^&text_size .sf ^&text_font .fg $1
.dv end_ex_cen_cfig .fo on .bc end .st ^&text_size .sf ^&text_font .fg x=center$1

.dv start_ex_cen  .bc start &ex_start 
.dv end_ex_cen  .fo on .bc end
.dv end_ex_cen_fig	.fo on .sp 1 .bc end .st ^&{text_size!10p} .sf ^&{text_font!Helvetica} .fg $1 ^:
.dv end_ex_cen_cfig	.fo on .sp 1 .bc end .st ^&{text_size!10p} .sf ^&{text_font!Helvetica} .fg x=center $1 ^:

.** backwards compatable names (deprecated)
.dv ex_start ^&half_space  .st ^&{ex_size!8p} .sf ^&cw_font .nf
.dv ex_end .fo on
.dv ex_end_fig .fo on .st ^&text_size .sf ^&text_font .fg $1
.dv ex_end_cfig .fo on .st ^&text_size .sf ^&text_font .fg x=center$1

.dv ex_start_cen  &ex_start 
.dv ex_end_cen  &ex_end


.** no concept of page, so always imbed the file ($2)
.dv ifroom .im $2

.** --------- notes and super scripts ---------------------------------
.** note adds a super script number automatically generated/increased
.** super allows user to supply the superscript text
.dv _cmd_note_num 1

.dv super .sm ^^<sup^^>$1^^</sup^^>
.dv super_sp  ^^<sup^^>$1^^</sup^^>

.dv note .sm ^^<sup^^>^&_cmd_note_num^^</sup^^> .dv _cmd_note_num ^[ %.0f ^&_cmd_note_num 1 + ] ^:
.dv note_sp  ^^<sup^^>^&_cmd_note_num^^</sup^^> .dv _cmd_note_num ^[ %.0f ^&_cmd_note_num 1 + ] ^:

.** no concept of a "page" so all column notes go to the end
.**  start_note(length)
.dv beg_note .cn start atend Times-roman 8p $1
.dv end_note .cn end
.dv show_notes .cn show

.** deprecated
.dv start_note .cn start atend Times-roman 8p $1

.** no concept of page, so always imbed the file ($2)
.dv ifroom .im $2

.** greek and other symbol defs
.im symbol_html.im
