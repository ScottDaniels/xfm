
.if false
	Mnemonic:	cmd_master.im
	Abstract:	This is the master imbed file for common macro definitions (CMD).
				Its only purpose is to pull in the correct cmd imbed file bsed
				on the user supplied output_type variable. If output_type is not
				defined, the environment is searched for XFM_OUTPUT_TYPE. If
				neither are defined, text is assumed.

				These variables may be set in the environment to influence processing:
					XFM_OUTPUT_TYPE
					XFM_IMBED_HTML

				and will be overridden if these corresponding variables are set by the
				user document before imbedding this code:
					output_type
					imbed_html

				Both of these variables will be set with environment or default values
				if not explicitly set in the user document.

				This code generates _cmd_* macros/defines, most of which are meant for
				internal consumption.

	Date:		20 June 2020
	Author:		E. Scott Daniels
.fi

.**  we require at least v2.2.2 for Match support in .if
.**
.dv _cmd_error "### ERROR ### using cmd_master.im requires {X}fm >= 2.2.2"
.dv _cmd_info  "              {X}fm version: &{_major}.&{_minor}.&{_patch}"
.gv semver

.** MUST do this early to prevent accidents with things we define here
.** imbed forward ref file if there
.gv e XFM_PASS _pass
.if &_pass 2 = 
	.im _fref.ca
.fi

.** build a version string from XFM defs allowing for two digit minor and patch values.
.dv _cmd_v [ &_major 10000 * &_minor 100 * + &_patch + ]
.if &_cmd_v 20202 <
	.sv _cmd_error
	.sv _cmd_info
	.qu
.fi

.** for any use that needs to know the version of the CMD set
.dv _cmd_major 2
.dv _cmd_minor 0
.dv _cmd_patch 0

.** ------ stupid things because MD can't handle <xxx> properly -----
.** put $1 in angle braces; overrridden in MD and maybe RST
.dv angle <$1>

.** ---- reference/citation support -------------------------------------------------------

.if false
	bib references are enabled with .dv bib_refs 1
	Macros:
		ref(name)	- define a reference; auto assigned a number
		cite(name)  - add [n] to the text where n matches the number assigned by ref
		cite_sp(name)	- same as site, but adds a space before [n]
		rent(name)	- start the reference list entry with [n]
.fi
.dv _ref_num 1
.dv ref .dv $1 ^&{_ref_num} ^:   .dv _ref_num ^[ %.0f ^&_ref_num 1 + ] ^:
.dv cite .ev ^`.sm ^^^[^&{$1}]` 
.dv cite_sp .ev ^` ^^^[^^^&{$1}]` 
.dv rent .ev ^`.di  ^^^[^^^&{$1}] ^:` 

.** ----------------------------------------------------------------------------------------------
.** set up common things that aren't needed in all -- before imbedding language specific stuff

.dv force_di_breaks

.dv font_size .st $1
.dv font .sf $1
.dv set_font_normal .sf ^&{text_font!Helvetica}

.dv ll .ll &{line_len!6.5i}
.dv cn_show .cn show

.** ----- default (no-ops) strike macros; real ones placed where supported in cmd_xxx.im files -----
.dv strikeWeight
.dv strike $2
.dv strikef $2
.dv beg_strike 
.dv end_strike 

.dv hilitef $2
.dv hilite $2
.dv beg_hilite
.dv end_hilite

.** ------------------------ no user macro defs below here --------------------------------------------
.** hfm and tfm output is single column, so this is the default
.dv one_col  .cd 1 &{col_width!8.0i} m=0i
.dv two_col  .cd 1 &{col_width!8.0i} m=0i

.** if caller sets output_type use it, else look for env var.
.** output type is meaningles for postscript/hfm formatters; output is
.** hard set in those cases. For tfm we check to see if it's in the env
.** and default to txt if not set and not in env.
.if pfm
	.dv output_type ps
	.if indent_amt
		.dv beg_cn .cn start atbot l=&{col_width!6.5i} i=&indent_amt ^&{note_font!Times-Roman} ^&{note_size!8p} $1
	.ei
		.dv beg_cn .cn start atbot ^&{note_font!Times-Roman} ^&{note_size!8p} $1
	.fi
	.dv end_cn .cn end

	.** back compat (deprecated)
	.dv cn_start .cn start atbot ^&{note_font!Times-Roman} ^&{note_size!8p} $1
	.dv cn_end .cn end

	.im cmd_ps.im
.ei
	.if hfm
		.dv output_type html
		.dv atbot atend
		.dv beg_cn .cn  start atclose $1
		.dv end_cn .cn end
	
		.** back compat
		.dv cn_start .cn  start atclose $1
		.dv cn_end .cn end
	
		.im cmd_html.im
	.ei
		.gv e XFM_OUTPUT_TYPE _cmd_ot
		.dv _cmd_ot &{_cmd_ot!TEXT}
		.dv output_type &{output_type!&_cmd_ot}

		.dv atbot atclose
		.dv beg_cn .cn  start atclose $1
		.dv end_cn .cn end
	
		.** deprecated; back compat
		.dv cn_start .cn  start atclose $1
		.dv cn_end .cn end
	
		.** some things might be supported for md/rst if imbedded html can be used (e.g. markdown super script)
		.gv e XFM_IMBED_HTML _cmd_imbed_html
		.dv _cmd_oimbed_htmlt &{_cmd_oimbed_htmlt!0}
		.dv imbed_html &{output_type!&_cmd_imbed_html}
	
		.if [ "&{output_type}" "text" "txt" "TEXT" "TXT" Match ]
			.im cmd_txt.im
		.fi
		.if [ "&{output_type}" "markdown" "md" "MARKDOWN" "MD" Match ]
			.im cmd_md.im
		.fi
		.if [ "&{output_type}" "rst" "RST" Match ]
			.im cmd_rst.im
		.fi
		.if [ "&{output_type}" "roff" "ROFF" Match ]
			.im cmd_roff.im
		.fi
	.fi
.fi


.** ----- appendix "lettering" support
.** Appendix header numbering and H1 tag of "Appendix X:" along with
.** page numbering style of <section>-<page> (e.g. A-1) with support
.** for getting the fancy page numbers back into the table of contents
.** when using PFM. If a different "tag" is desired, it can be set
.** after the start_appendix invocation with something like this:
.**		.hn alpha `f=Exhibit %c: `
.** The back quotes are required if there are spaces in the tag and
.** the f= MUST be inside of the back quotes.
.**
.dv beg_appendix .pa .hn alpha 0 ^: .gv sect
.dv _alphaPNum .gv sect  .ev ^`.pn on center noline fmt=^^^&{_sect}-%d  0^`
.dv _alphaTocNum .pn on center noline tfmt=^&{_next_h1}-%d 0
.dv ah1 ^&_alphaTocNum  ^: .h1 $1 ^: ^&_alphaPNum 

.** ---- these are last because things like indent_amt might be specific to output type

.dv bold_font &{bold_font!Helvetica-Bold}
.dv cw_font &{cw_font!Courier}
.dv cw_size &{cw_size!8p}
.dv ital_font &{ital_font!Helvetica-Oblique}
.dv text_font &{text_font!Helvetica}
.dv text_size &{text_size!10}
.dv note_font &{note_font!Times-roman}
.dv note_size &{note_size!8p}
.dv header_font &{header_font!Helvetica-Bold}
.dv set_font_size .st $1

.dv indent_amt &{indent_amt!.5i}

.** sane header setup, the user can always reset any of these; sizes based on text size
.dv _h1ps [ &text_size 6 + ]
.dv _h2ps [ &text_size 2 +  ]
.dv _h3ps [ &text_size 2 +  ]
.dh 1 s=2,1 e=no i=0 p=&_h1ps f=&header_font m=&indent_amt
.dh 2 s=1,.3 e=no i=0 &_h2ps f=&header_font m=&indent_amt
.dh 3 s=1,0 e=no i=0 &_h3ps f=&header_font m=&indent_amt


.** ------- last things to set environment --------
&font( &text_font )
&font_size( &text_size )
&one_col
