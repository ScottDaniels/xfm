

 IMBED DIRECTORY
 ===============

 This directory contains several 'startup' imbed files or 
 templates which attempt to provide some 'boiler plate' 
 definitions. Some, like doc_start, or hfmstart, are more 
 elaborate and failry complete setups while there are smaller 
 files like markdown which have a specific purpose and can be 
 combined with others. 
  
 This directory also contains a master mkfile which can be 
 imported into mkfiles in order to provide standard/basic rules 
 for doc generation (e.g. to generate a .pdf, do this with the 
 same named .xfm source). 
  
 There are several README files in this directory. Each is 
 generated with the README.xfm source and uses the 
 *Common Macro Definitionsdescribed below. This allows the same 
 {X}fm soruce to generate multiple output formats and the 
 README.xfm is a fair example of how to use most of the common 
 macros.. 


 Common Macro Definitions
 ------------------------

 With the insistance of generating "documents" in some kind of 
 plain text format that is converted to HTML on the fly (e.g. 
 markdown or RST), and to support converting {X}fm source to 
 other real formatter source (e.g. ROFF), the use of common macro 
 definitions is receommended to allow {X}fm source to generate 
 both postscript output via pfm, and the desired plain text 
 output via tfm with the special markings for the chosen parser. 
  
 The common macros are defined in output specific startup files: 
  
    
   * cmd_md.im 
      
   * cmd_rst.im 
      
   * cmd_roff.im 
      
   * cmd_ps.im 
      
   * cmd_txt.im 
    
  
 The Common Macro Definition (cmd_) files are similar to the 
 original output specific imbed files (e.g. markdown.im, and 
 rst.im). The original files (which still remain in the repo for 
 backwards compatability) were developed independently, and in 
 some cases, were inconsistent to the point that it was 
 impossible to generate two kinds of output from the same source. 
 The CMD files may be imbedded directly, or the variable 
 output_type may be set and the master CMD file (cmd_master.im) 
 can be imbedded. If using the output_type variable, the 
 following types are supported (and are case sensitive inasmuch 
 as all upper or lower case letters must be used): 
  
    
   * MARKDOWN 
      
   * RST 
      
   * ROFF 
      
   * TXT 
      
   * POSTSCRIPT or PS 
    
  
 If the output type is not supplied in the environment variable, 
 then the 
 *cmd_masterfile will assume "TXT" if the formatter is 
 ``tfm`` and "PS" if the formatter is 
 ``pfm.`` Refer to the mkfile for examples of how to invoke the 
 proper formatter with the right environment variables. 
  


 VARIABLES
 =========

 The following variables may be set in the user document and 
 affect some aspect of processing when a macro is executed. 
  
    
       .. list-table:: 
         :widths: 15,80 
         :header-rows: 0 
         :class: borderless 
          
         * - **col_indent** 
           - 
             The amount that the left (or only) column is indented from 
             the page edge. 
              
             | 
          
         * - **col_width** 
           - 
             The width used when either the &one_col or &two_col macros is 
             used. 
              
             | 
          
         * - **ex_font** 
           - 
             The font used for example text when the output type supports 
             font changes. 
              
             | 
          
         * - **ex_size** 
           - 
             This is the size that example text should be rendered in when 
             possible. 
              
             | 
          
         * - **indent_amt** 
           - 
             The amount of space used to indent the left margin. This 
             variable must be set prior to imbedding the cmd file. 
              
             | 
          
         * - **text_size** 
           - 
             This is the size that text should be rendered in when 
             possible. It is the size that is restored following an 
             example. 
              
             | 
          
         * - **text_font** 
           - 
             The font that text should be rendered using. 
              
    


 MACROS
 ======

 The following macros are defined and should be consistent across 
 all of the CMD files. Some output formats might not support all 
 features (e.g. tables are not supported in plain text output). 
  
    
       .. list-table:: 
         :widths: 15,80 
         :header-rows: 0 
         :class: borderless 
          
         * - **beg_dlist(pfm:pfm:rst:txt)** 
           - 
             Start a definition list. Parameters vary based on output 
             type. $1 and $2 are used by pfm for postscript output. $3 is 
             used when output is for RST, and $4 is used for plain text 
             output. As an example: 
              
             :: 
              
                   &beg_dlist( 1.5i : Helvetica-bold : 15,80 : 1i )
              
              
              
             | 
          
         * - **beg_list(<lic>)** 
           - 
             Start a list item list (bullet list). <lic> is used as the 
             bullet character and has different meaning based on output 
             type. The variables &lic1, &lic2, and &lic3 are defined as 
             best suited for the current output type and thir use is 
             stongly recommended. 
              
              
             | 
          
         * - **beg_table(pfm-opts** 
           - 
             Start a table with borders. Tables are supported only for 
             postscript and RST output and may require options to be 
             passed in the parameter list. These are likely column widths 
             etc. 
              
              
             | 
          
         * - **beg_table_nb(pfm-opts:rst-opts)** 
           - 
             Start a table without borders. The same options apply as 
             described for the &beg_table macro. For some output types 
             tables are not supported. 
              
              
             | 
          
         * - **bold(<text>)** 
           - 
             Render text in bold font. This uses &bold_font if defined. 
              
              
             | 
          
         * - **break** 
           - 
             Stop formatting and break the output line. The next text will 
             be placed at the left margin on the following line. 
              
              
             | 
          
         * - **center(<text>)** 
           - 
             Center <text>. If <text> is numeric, it must be escaped with 
             a carrot (^). 
              
              
             | 
          
         * - **center_end** 
           - 
             End block center. 
              
              
             | 
          
         * - **center_start** 
           - 
             Begin block center. All text between this macro and the 
             &center_end macro will be centered between the current 
             margins. 
              
              
             | 
          
         * - **col(<opts>)** 
           - 
             Move to the next column in current table. For postscript 
             output any options needed may be supplied. 
              
              
             | 
          
         * - **cw(<text>)** 
           - 
             Render text in constant width font. This uses the &cw_font 
             variable if defined. 
              
              
             | 
          
         * - **di(term)** 
           - 
             Start next defintion for term. Causes a line break and then 
             writes the term in the font given on the &beg_dlist macro (or 
             bold if the output type doesn't support fonts). Terms are 
             written against the left margin with definition text indented 
             by the amount given on the &beg_dlist macro. 
              
              
             | 
          
         * - **di_space(term)** 
           - 
             Same as &di, execpt it adds a blank line after the previous 
             term. 
              
              
             | 
          
         * - **end_dlist** 
           - 
             Ends the definition list by causing a line break and 
             returning the left margin to the position it held before the 
             definition list started. 
              
              
             | 
          
         * - **end_list** 
           - 
             End an item list (bullet list). The left margin is returned 
             to its setting prior to the start of the list. 
              
              
             | 
          
         * - **end_note** 
           - 
             End a column note. The column note is added to the list which 
             will be included either at the end of the current column 
             (page) for postscript, or at the end of the document. 
             Inclusion on the next column eject is automatic, while 
             inclusion at the end of the document must be forces by using 
             the &show_notes macro. 
              
              
             | 
          
         * - **end_table** 
           - 
             Close the current table. 
              
              
             | 
          
         * - **esc(<text>)** 
           - 
             Add an escape character before <text>. 
              
              
             | 
          
         * - **ex_end** 
           - 
             End an example. Restores font size to &text_size and font to 
             &text_font. 
              
              
             | 
          
         * - **ex_end_cfig(<text>)** 
           - 
             End an example and add <text> as a centered figure label. 
             Restores font and font size as described for 
             ``ex_end.`` 
              
              
             | 
          
         * - **ex_end_fig(<text>)** 
           - 
             End an example and add <text> as a figure label Restores font 
             and font size as described for 
             ``ex_end.`` 
              
              
             | 
          
         * - **ex_start** 
           - 
             Start an example by skipping half space, setting text to 
             &example_font and text size to &example_size 
              
              
             | 
          
         * - **fig(<text>)** 
           - 
             Generate a figure label 
              
              
             | 
          
         * - **fig_cen(<text>)** 
           - 
             Generate a centered figure label 
              
              
             | 
          
         * - **fo** 
           - 
             Start formatting (must be in col 0) 
              
              
             | 
          
         * - **h1(<text>)** 
           - 
             Header level 1 using <text> as the header. 
              
              
             | 
          
         * - **h2(<text>)** 
           - 
             Header level 2 using <text as the header. 
              
              
             | 
          
         * - **h3(<text>)** 
           - 
             Header level 3 using <text as the header. 
              
              
             | 
          
         * - **h4(<text>)** 
           - 
             Header level 4 using <text as the header. 
              
              
             | 
          
         * - **half_space** 
           - 
             Generate a blank line with 1/2 normal distance if the output 
             format supports this. If not supported a normal space is 
             generated. 
              
              
             | 
          
         * - **head_num(on|off)** 
           - 
             Enable or disable header numbering 
              
              
             | 
          
         * - **indent** 
           - 
             Indent text by &indent_delta or .25i if not defined. Pair 
             with &uindent. 
              
              
             | 
          
         * - **ital(<text>)** 
           - 
             Render <text> in italic font. 
              
              
             | 
          
         * - **just(on|off)** 
           - 
             Turn justification on or off. 
              
              
             | 
          
         * - **li** 
           - 
             Break the current line, then places a list item (bullet item) 
             character to the right of of the text starting on the next 
             line. 
              
              
             | 
          
         * - **li_space** 
           - 
             Same as &li, but inserts a half space rather than just a line 
             break. 
              
              
             | 
          
         * - **lic1** 
           - 
             List item character 1. May be passed to the &beg_list maccro. 
              
              
             | 
          
         * - **lic2** 
           - 
             List item character 2. May be passed to the &beg_list maccro. 
              
              
             | 
          
         * - **lic3** 
           - 
             List item character 3 May be passed to the &beg_list maccro. 
              
              
             | 
          
         * - **line_len(n)** 
           - 
             Set line length to n. The value n should be suffixed with 
             either 'i' (inches) or 'p' points. E.g. &line_len(6.5i). 
              
              
             | 
          
         * - **mult_space(n)** 
           - 
             Generate multiple (n) blank lines. 
              
              
             | 
          
         * - **nf** 
           - 
             No formatting; turns formatting off until &fo is encountered 
             in the first column of the input. 
              
              
             | 
          
         * - **note** 
           - 
             Add a note number (super script if supported). The number is 
             automatically increased such that the next use of &note will 
             produce the next number. 
              
              
             | 
          
         * - **row(pfm-opts)** 
           - 
             Next row in current table. For pfm output, the row options 
             (e.g. l=2) can be supplied as parameters. 
              
              
             | 
          
         * - **set_font_bold** 
           - 
             Set font to bold for all subsequent text until another set 
             font macro is encountered. 
              
              
             | 
          
         * - **set_font_cw** 
           - 
             Set font to constant width for all subsequent text until 
             another set font macro is encountered. 
              
              
             | 
          
         * - **set_font_ital** 
           - 
             Set font to italic for all subsequent text until another set 
             font macro is encountered. 
              
              
             | 
          
         * - **space** 
           - 
             Break the current output, and generate a blank line between 
             it and the next output. 
              
              
             | 
          
         * - **show_notes** 
           - 
             Show any column notes which were deferred until the 
             invocation of this macro (notes with 
             *at end). 
              
             | 
          
         * - **start_note(n)** 
           - 
             Start a column note. The value n is the space reserved at the 
             end of the page (e.g. .5i to reserve one half inch). Pair 
             with &end_note. 
              
              
             | 
          
         * - **super(<text>)** 
           - 
             Adds <text> as a superscript. No horizontal white space is 
             added. 
              
              
             | 
          
         * - **super_space** 
           - 
             Adds <text> as a superscript. A single space is added between 
             the previous token and the superscript. 
              
              
             | 
          
         * - **uindent** 
           - 
             unindent text 
              
    
  
