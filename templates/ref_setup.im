
.if false
	Mnemonic:	ref_setup.im
	Abstract:	This defines a set of macros that can assist with defining references
				and including them with minimal effort. The largest benefit is the
				ability to add references as the document grows, inserting them in
				alpha order, without the need to rework the reference numbers
				and without the bloody [Eldridge04] style reference in the text
				(which I belive was some standard's way to avoid this when manual
				typewriters were used).

				First, create reference entries in a single file:

				&beg_references
				&ref_def( eldridge )
					Eldridge, K. E., Jain,  S. K., 1976.
 					&ital(Ring Theory: Proceedings of the Ohio University Conference)
					Marcel Dekker Inc. USA.
				&end_ref
				.** as many others as are needed
				&end_references

				or place each "ref-def" into it's own file and create a single file
				which imbeds the references:

				&beg_references
				.im &{bib_dir}/eldridge_string.bib
				.im &{bib_dir}/joshi_draco.bib
				:
				:
				&end_references

				(the latter method is more flexible when references are being shared
				by more than one document.) In either case, the refernces must live in
				a separate file to make this easy.

				Next, imbed this macro set and  the xxx.ref file, or references in order, 
				at the beginning of the document source.  This will create the variables 
				(e.g. &eldridge). This can be done using the &init_refs() macro and giving
				the file name as the parameter.

				Use the variables in the document:
					...is a commonly attributed to early ring theory [&eldridge].

				At the place in the document where the references are to be detailed, 
				the &expand_refs() macro can be used, again giving it the file name
				like this:

					&expand_refs(xxx.ref)

				The references are created with an underlying defintion list which has
				a column size of .25i by default. Thwi works well with 10 and 12 point
				fonts, but for larger fonts, more room might be needed, and this can
				be changed by setting the ref_col_size variable before using the
				&expand_refs() macro.

	Date:		28 September 2021
	Author:		E. Scott Daniels
.fi

.** the reference number for next use
.dv _rnum 1

.** the ensured space for each def when expanding them into the output
.dv _ref_def_space 1i

.** ref "maps" $1 (a name) to the reference number allowing [&name] to be used in the src
.** resulting in [1] in the output. Names can be added/removed from the list (ref_def)
.** allowing their number to adjust without requiring a remap across the source.
.dv _ref .dv $1 ^&_rnum ^: .dv _rnum ^[ %.0f ^&_rnum 1 + ] ^: 

.** ref_def captures the name and maps it to a ref number. if expanding then the text
.** between ref_def and and end_ref is added.
.**
.dv ref_def .ev ^`^&_ref( $1 ) .if  ^^&_expand_refs 0 > ^: .sp .4 .cc ^&_ref_def_space .di ^^&${*1}. ^: ^`
.dv end_ref .fi

.** needed to setup and cleanup round the ref_def/end_ref pairs
.**
.dv beg_references .dv _rnum 1 ^: .if &_expand_refs 0 > ^: .bd ^&{ref_col_size!.25i} &text_font ^: .ju off .fi
.dv end_references .if &_expand_refs 0 > ^: .ed .sp .5 .fi

.** convenience macros for the caller to imbed their reference file with
.**
.dv init_refs .im $1
.dv expand_refs .dv _expand_refs 1 ^: .im $1
