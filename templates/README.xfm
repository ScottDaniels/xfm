.if false
	Mnemonic:	README.xfm
	Abstract:	Source that generates all flavours of the readme.

				This source serves as an example of how to use the common
				macro definions (cmd_*.im) source to generate various
				flavours of ASCII output (markdown, rst, etc.) from the
				same input source.

	Date:		16 June 2020
	Author:		E. Scott Daniels
.fi

.dv indent_amt .5i
.dv text_font Helvetica
.dv text_size 10p
.dv ex_size 8p
.dv ex_font Courier
.dv col_width 6.5i
.dv col_indent .5i

.im cmd_master.im

&head_num( off )
&one_col

&h1(Imbed Directory)
This directory contains several 'startup' imbed files or templates
which attempt to provide some 'boiler plate' definitions. Some, like
doc_start, or hfmstart, are more elaborate and failry complete setups
while there are smaller files like markdown which have a specific
purpose and can be combined with others.
&space

This directory also contains a master mkfile which can be imported
into mkfiles in order to provide standard/basic rules for doc
generation (e.g. to generate a .pdf, do this with the same named
.xfm source).
&space

There are several README files in this directory. Each is generated
with the README.xfm source and uses the &ital(Common Macro Definitions)
described below.
This allows the same {X}fm soruce to generate multiple output formats
and the README.xfm is a fair example of how to use most of the common
macros..

&h2(Common Macro Definitions)
With the insistance of generating "documents" in some kind of plain
text format that is converted to HTML on the fly (e.g. markdown or
RST), and to support converting {X}fm source to other real formatter
source (e.g. ROFF), the use of common macro definitions is receommended
to allow {X}fm source to generate both postscript output via pfm, and
the desired plain text output via tfm with the special markings for
the chosen parser.
&space

The common macros are defined in output specific startup files:
&half_space
&indent
&beg_list(&lic1)
	&li cmd_md.im
	&li_space cmd_rst.im
	&li_space cmd_html.im
	&li_space cmd_roff.im
	&li_space cmd_ps.im
	&li_space cmd_txt.im
&end_list
&uindent
&space

The Common Macro Definition (cmd_) files are similar to the original
output specific imbed files (e.g. markdown.im, and rst.im). 
The original files (which still remain in the repo for backwards 
compatability) were developed independently, and
in some cases, were inconsistent to the point that it was impossible
to generate two kinds of output from the same source. The CMD files
may be imbedded directly, or the variable output_type may be set
and the master CMD file (cmd_master.im) can be imbedded.
If using the output_type variable, the following types are
supported (and are case sensitive inasmuch as all upper or lower
case letters must be used):

&half_space
&indent
&beg_list(&lic1)
	&li 		MARKDOWN
	&li_space	RST
	&li_space	ROFF
	&li_space	TXT
	&li_space	POSTSCRIPT or PS
&end_list
&uindent
&space

If the output type is not supplied in the environment variable, then
the &ital(cmd_master) file will assume "TXT" if the formatter is &cw(tfm)
and "PS" if the formatter is &cw(pfm.)
Refer to the mkfile for examples of how to invoke the proper formatter with
the right environment variables.
&space

&h1(Variables)
The following variables may be set in the user document and affect some
aspect of processing when a macro is executed.

&half_space
&indent
&beg_dlist( 1i &bold_font : : 15,80 : 1i )
&di_space(col_indent)
	The amount that the left (or only) column is indented from the page edge.

&di_space(col_width)
	The width used when either the ^&one_col or ^&two_col macros is used.

&di_space(ex_font)
	The font used for example text when the output type supports font
	changes.

&di_space(ex_size)
	This is the size that example text should be rendered in when possible.

&di_space(indent_amt)
	The amount of space used to indent the left margin.
	This variable must be set prior to imbedding the cmd file.

&di_space(text_size)
	This is the size that text should be rendered in when possible.
	It is the size that is restored following an example.

&di_space(text_font)
	The font that text should be rendered using.
&end_dlist
&uindent

.** -----------------------------------------------------------------------
&h1(Macros)
The following macros are defined and should be consistent across
all of the CMD files. 
Some output formats might not support all features (e.g. tables are
not supported in plain text output).
&space

&indent
&beg_dlist( 1.5i : : 15,80 : 1i )
&di(beg_dlist(pfm^:pfm^:rst^:txt))
	Start a definition list. 
	Parameters vary based on output type. 
	$1 and $2 are used by pfm for
	postscript output.
	$3 is used when output is for RST, and $4 is used for plain text output.
	As an example:
.sv ex_start
	&ex_start
    &beg_dlist( 1.5i : Helvetica-bold : 15,80 : 1i )
&ex_end
&half_space

&di(beg_list(<lic>))
	Start a list item list (bullet list). 
	<lic> is used as the bullet character and has different meaning based on
	output type.
	The variables ^&lic1, ^&lic2, and ^&lic3 are defined as best suited for
	the current output type and thir use is stongly recommended.
&half_space
.xx 0

&di(beg_table(pfm-opts:rst-opts))
	Start a table with borders. 
	Tables are supported only for postscript and RST output and may require
	options to be passed in the parameter list.
	These are likely column widths etc.
&half_space

&di(beg_table_nb(pfm-opts^:rst-opts))
	Start a table without borders.
	The same options apply as described for the ^&beg_table macro.
	For some output types tables are not supported.
&half_space

&di(bold(<text>))
	Render text in bold font.
	This uses ^&bold_font if defined.
&half_space

&di(break)
	Stop formatting and break the output line.
	The next text will be placed at the left margin on the following line.
&half_space

&di(center(<text>))
	Center <text>.
	If <text> is numeric, it must be escaped with a carrot (^^).
&half_space

&di(center_end)
	End block center.
&half_space

&di(center_start)
	Begin block center. All text between this macro and the ^&center_end macro
	will be centered between the current margins.
&half_space

&di(col(<opts>)
	Move to the next column in current table.
	For postscript output any options needed may be supplied.
&half_space

&di(cw(<text>))
	Render text in constant width font.
	This uses the ^&cw_font variable if defined.
&half_space

&di(di(term))
	Start next defintion for term.
	Causes a line break and then writes the term in the font given on the ^&beg_dlist macro (or bold if the
	output type doesn't support fonts).
	Terms are written against the left margin with definition text indented by the amount
	given on the ^&beg_dlist macro.
&half_space

&di(di_space(term))
	Same as ^&di, execpt it adds a blank line after the previous term.
&half_space

&di(end_dlist)
	Ends the definition list by causing a line break and returning the left margin
	to the position it held before the definition list started.
&half_space

&di(end_list)
	End an item list (bullet list).
	The left margin is returned to its setting prior to the start of the list.
&half_space

&di(end_note)
	End a column note.
	The column note is added to the list which will be included either at the end of
	the current column (page) for postscript, or at the end of the document.
	Inclusion on the next column eject is automatic, while inclusion at the end of the
	document must be forces by using the ^&show_notes macro.
&half_space

&di(end_table)
	Close the current table.
&half_space

&di(esc(<text>))
	Add an escape character before <text>.
&half_space

&di(ex_end)
	End an example. 
	Restores font size to ^&text_size and font to ^&text_font.
&half_space

&di(ex_end_cfig(<text>)) 
	End an example and add <text> as a centered figure label.
	Restores font and font size as described for &cw(ex_end.)
&half_space

&di(ex_end_fig(<text>))
	End an example and add <text> as a figure label
	Restores font and font size as described for &cw(ex_end.)
&half_space

&di(ex_start)
	Start an example by skipping half space, setting text to ^&example_font
	and text size to ^&example_size
&half_space

&di(fig(<text>))
	Generate a figure label
&half_space

&di(fig_cen(<text>))
	Generate a centered figure label
&half_space

&di(fo)
	Start formatting (must be in col 0)
&half_space

&di(h1(<text>))
	Header level 1 using <text> as the header.
&half_space

&di(h2(<text>))
	Header level 2 using <text as the header.
&half_space

&di(h3(<text>))
	Header level 3 using <text as the header.
&half_space

&di(h4(<text>))
	Header level 4 using <text as the header.
&half_space

&di(half_space)
	Generate a blank line with 1/2 normal distance if the output format
	supports this. If not supported a normal space is generated.
&half_space

&di(head_num(on|off))
	Enable or disable header numbering
&half_space

&di(indent)
	Indent text by ^&indent_delta or .25i  if not defined.
	Pair with ^&uindent.
&half_space

&di(ital(<text>))
	Render <text> in italic font.
&half_space

&di(just(on|off))
	Turn justification on or off.
&half_space

&di(li)
	Break the current line, then
	places a list item (bullet item) character to the right of of the 
	text starting on the next line.
&half_space

&di(li_space)
	Same as ^&li, but inserts a half space rather than just a line break.
&half_space

&di(lic1)
	List item character 1. 
	May be passed to the ^&beg_list maccro.
&half_space

&di(lic2)
	List item character 2.
	May be passed to the ^&beg_list maccro.
&half_space

&di(lic3)
	List item character 3
	May be passed to the ^&beg_list maccro.
&half_space

&di(line_len(n))
	Set line length to n. The value n should be suffixed with either 'i' (inches)
	or 'p' points. E.g. ^&line_len(6.5i).
&half_space

&di(mult_space(n))
	Generate multiple (n) blank lines.
&half_space

&di(nf)
	No formatting; turns formatting off until ^&fo is encountered in the first column
	of the input.
&half_space

&di(note)
	Add a note number (super script if supported). The number is automatically increased
	such that the next use of ^&note will produce the next number.
&half_space

&di(row(pfm-opts))
	Next row in current table. 
	For pfm output, the row options (e.g. l=2) can be supplied as parameters.
&half_space

&di(set_font_bold)
	Set font to bold for all subsequent text until another set font macro is encountered.
&half_space

&di(set_font_cw)
	Set font to constant width for all subsequent text until another set font macro is encountered.
&half_space

&di(set_font_ital)
	Set font to italic for all subsequent text until another set font macro is encountered.
&half_space

&di(space)
	Break the current output, and generate a blank line between it and the next output.
&half_space

&di(show_notes)
	Show any column notes which were deferred until the invocation of this macro (notes with
	&ital(at end) .sm ).

&di(start_note(n))
	Start a column note. The value n is the space reserved at the end of the page
	(e.g. ^.5i to reserve one half inch).
	Pair with ^&end_note.
&half_space

&di(super(<text>))
	Adds <text> as a superscript.
	No horizontal white space is added.
&half_space

&di(super_space)
	Adds <text> as a superscript.
	A single space is added between the previous token and the superscript.
&half_space

&di(uindent)
	unindent text

&end_dlist
&uindent
&space
