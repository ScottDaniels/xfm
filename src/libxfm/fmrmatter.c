/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

/*

	Mnemonic:  FMrmatter
	Abstract:  Allows running matter (headers/footers) to be controlled with one command.
					.rm f=<font> s=<font-size> m=<margin>

		Date:      27 December 2019
		Author:    E. Scott Daniels
*/

extern void FMrmatter( void ) {
 	char *buf;          			// pointer at the next portion of the buffer to work with
 	int len;            			// len of parameter

	while( (len = FMgetparm( &buf )) > 0 ) {
		switch( *buf )
		{
			case 'f':										// font
				if( runfont != NULL ) {
					free( runfont );
				}
				runfont = strdup( buf+2 );					// skipping f=
				break;

			case 'l':										// left header margin from page/column edge
				runlmar = FMgetpts( buf+2, len-2 );
				break;
				
			case 'r':										// right header margin from page/column edge
				runrmar = FMgetpts( buf+2, len-2 );
				break;

			case 's':										// font size
				len= FMgetpts( buf+2, len-2 );
				if( len > 0 ) {
					runsize = len;
				}
				break;
		}
	}

	TRACE( 2, "running matter: font=%s size=%d rmargin=%d lmargin=%d\n", runfont, runsize, runrmar, runlmar );
}
