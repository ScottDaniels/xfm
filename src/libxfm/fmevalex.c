	/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

/*
**************************************************************************
*
*  Mnemonic: FMevalex
*  Abstract: This routine will evaluate a reverse polish expression and
*            return the character equivalent of the result. The
*            expression may contain constant and already defined variables.
*            Expressions have the following format:
*               [ &var &var + ]   (Add var to var)
*  Returns:  The integer result of the expression
*  Parms:    Optional - buf - Pointer to buffer to convert result into
*  Date:     28 March 1993
*  Author:   E. Scott Daniels
*
*  Modified: 21 Apr 1997 - To calc in floating point
*            22 Apr 1997 - To truncate trailing 0s from string.
*            10 Jun 1997 - To add TIME command
*                          To add FRAC command
*                          To add INT command
*				17 Jul 2016 - Changes for better prototype generation.
*			08 Jun 2020 - Add ave, min and max; reformatted
*			11 Jun 2020 - Add count and cpop
**************************************************************************
*/

static int eval_warned = 0;			// issued warning about ?% being deprecated

/*
	Given two semantic version numbers (e.g. 1.2.3) return true if sv1 <= sv2.
	or v2 is the same or newer than version 1.
	This supports evaled [ "2.3.4" VGE] ]  ( current version greater/equal than 2.3.4 )
*/
static int vge( char* sv1, char* sv2 ) {
	char*	tok1;
	char*	tok2;
	int		v1;
	int		v2;

	if( sv1 == NULL && sv2 == NULL ) {		// if we recursed to the end; then they are == which is true
		return 1;
	}

	if( sv1 == NULL || sv2 == NULL ) {		// both strings must have same number of notes or it's false
		return 0;
	}

	v1 = atoi( sv1 );
	v2 = atoi( sv2 );
	if( v1 < v2 ) {
		return 0;
	}
	if( v1 > v2 ) {
		return 1;
	}

	if( (tok1 = strchr( sv1, '.' )) != NULL ) {
		tok1++;
	}
	if( (tok2 = strchr( sv2, '.' )) != NULL ) {
		tok2++;
	}

	return vge( tok1, tok2 );			// node values are ==, recurse to compare next
}

extern int FMevalex( char* rbuf, int rbuf_len ) {
	int i;             /* integer value */
	int sp = 0;        /* stack pointer */
	int ssp = 0;       // string stack pointer (index)
	char *idx;         /* pointer into string */
	char	wbuf[512];
	double stack[100]; /* stack */
	char*	sstack[100];	// string stack for match
	char *buf;         /* pointer at next token to process */
	char fmt[100];     /* format string */
	int usr_fmt = 0; 
	int	count;

	snprintf( fmt, sizeof( fmt ), "%%.4f" );   /* default format string */

	stack[0] = 0;

	while( FMgetparm( &buf ) ) {
		TRACE( 3, "expr: working on (%s)\n", buf );

		switch( *buf ) {
			case 'A':                      // compute average of everything on the stack
				count = 1;
				while( sp > 1 ) {
					count++;
					stack[sp-2] += stack[sp-1];   /* add top two and push */
					sp--;                         /* one less thing on stack */
					TRACE( 2, "summing for average: [%d] = %.3f count=%d\n", sp, stack[sp], count );
				}
				sp--;
				TRACE( 2, "sum on stack: [%d] = %.3f count=%d\n", sp, stack[sp], count );
				if( count > 0 && sp >= 0 ) {
					stack[0] = stack[0] / (double) count;
				} else {
					stack[0] = 0;
				}
				sp = 1;
				break;

			case 'C':						// count, cpop or CVGE
				if( *(buf+1) == 'o' || *(buf+1) == 'P' ) {			 // Count or CPop 
					TRACE( 2, "count on stack = %d\n", sp );
					stack[0] = sp;
					sp = 1;
				} else {					// assume CVLE for now
					if( ssp > 0 ) {
						TRACE( 2, "vle: checking current version against: '%s'\n", sstack[ssp-1] );
					} else {
						TRACE( 2, "vle: nothing pushed\n" );
						stack[0] = 0;
						ssp = 1;
						break;
					}
	
					snprintf( wbuf, sizeof( wbuf ), "%d.%d.%d", major_ver, minor_ver, patch_level );	// our current version
					stack[sp] = vge( wbuf, sstack[ssp-1] ); 
					sp++;
					ssp--;
				}
				break;

			case 'E':						// exchange top two elements
				if( sp > 1 ) {
					count = stack[sp-1];
					stack[sp-1] = stack[sp-2];
					stack[sp-2] = count;
				}
				break;

			case 'F':                      /* leave fractial portion of top */
				i = stack[sp-1];
				stack[sp-1] = stack[sp-1] - i;
				break;

			case 'M':
				sp--;
				count = stack[sp];
				if( toupper( *(buf+1) ) == 'I' ) {		// find min
					while( sp > 0 ) {
						if( stack[sp-1] < count ) {
							TRACE( 2, "new min: [%d] = %.3f\n", sp, stack[sp-1] );
							count = stack[sp-1];
						}
						sp--;
					}
				} else {
					if( strlen( buf ) >= 3 && toupper( *(buf+2) ) == 'X' ) {
						while( sp > 0 ) {
							if( stack[sp-1] > count ) {
								TRACE( 2, "new max: [%d] = %.3f\n", sp, stack[sp-1] );
								count = stack[sp-1];
							}
							sp--;
						}
					} else {			// MATCH 
						ssp--;
						if( ssp > 1 ) {
							TRACE( 2, "match looking for: '%s'\n", sstack[0] );
						} else {
							TRACE( 2, "match nothing pushed\n" );
						}
						count = 0;									// count set to 1 on match
						while( count == 0 && ssp > 0 ) {			// while something to compare or a match
							TRACE( 2, "match compare '%s'\n", sstack[ssp] );
							if( strcmp( sstack[0], sstack[ssp] ) == 0 ) {
								count = 1;
							}

							ssp--;                         /* one less thing on stack */
						}
					}
				}
						
				sp = 1;
				stack[0] = count;
				break;

			case 'I':                      /* make top integer */
				i = stack[sp-1];
				stack[sp-1] = i;
				break;

			case 'S':                      /* sum all elements on the stack */
				while( sp > 1 ) {
					stack[sp-2] += stack[sp-1];   /* add top two and push */
					sp--;                         /* one less thing on stack */
				}
				break;

			case 'T':                      /* convert top of stack to time */
				i = stack[sp-1];              /* make int */
				stack[sp-1] = i + ((stack[sp-1] - i) * 0.60);
				break;

			case '+':
				if( isdigit( *(buf+1) ) ) {
					stack[sp++] = atof( buf ); 		/* assume +n and push on stack */ 
				}else {
					if( sp < 2 )
						break;
					if( trace ) {
						fprintf( stderr, "eval: [%.3f %.3f +] = ", stack[sp-2], stack[sp-1] );
					}
					stack[sp-2] += stack[sp-1];   /* add top two and push */
					sp--;                         /* one less thing on stack */
					if( trace ) {
						fprintf( stderr, "%.3f\n", stack[sp-1] );
					}
				}
				break;

			case '-':
				if( isdigit( *(buf+1) ) ) {
					stack[sp++] = atof( buf );  		/* assume -n and push on stack */
				} else {
					if( sp < 2 )
						break;
					if( trace )
						fprintf( stderr, "eval: [%.3f %.3f -] = ", stack[sp-2], stack[sp-1] );
					stack[sp-2] = stack[sp-2] - stack[sp-1];   /* sub top two and push */
					sp--;                         /* one less thing on stack */
	
					if( trace )
						fprintf( stderr, "%.3f\n", stack[sp-1] );
				}
				break;

			case '*':
				if( sp < 2 )
					break;

				if( trace )
					fprintf( stderr, "eval: [%.3f %.3f *] = ", stack[sp-2], stack[sp-1] );

				stack[sp-2] *= stack[sp-1];   /* mul top two and push */

				if( trace )
					fprintf( stderr, "%.3f\n", stack[sp-1] );
				sp--;                         /* one less thing on stack */
				break;

			case '/':
				if( sp < 2 )
					break;

				if( stack[sp-1] != 0 ) {
					if( trace )
						fprintf( stderr, "eval: [%.3f %.3f /] = ", stack[sp-2], stack[sp-1] );
					stack[sp-2] = stack[sp-2]  / stack[sp-1];   /* div top two and push */
					if( trace )
						fprintf( stderr, "%.3f\n", stack[sp-2] );
				} else {
					if( trace ) 
						fprintf( stderr, "eval: [%.3f %.3f /] = <NAN>\n", stack[sp-2], stack[sp-1] );
					fprintf( stderr, "division by zero detected -- bad values generated on stack\n" );
					stack[sp-2] = 0;
					sp--;
				}
				sp--;                         /* one less thing on stack */
				break;

			case '%':					// mod top two, or assume it's a format if nothing on stack
				if( *(buf+1) == 0 ) {
					if( stack[sp-1] != 0 ) {
						stack[sp-2] = (int) stack[sp-2] % (int) stack[sp-1];
					} else {
						stack[sp-2] = 0;
						fprintf( stderr, "modular division by zero detected -- bad values generated on stack\n" );
					}
					sp--;
				} else {
					usr_fmt++;
					strncpy( fmt, buf, sizeof( fmt )-1 );
				}
				break;

			case '?':				// e.g. %.0f can now be placed anywhere convenient so this is decprecated
				if( ! eval_warned ) {
					FMmsg( E_DEPRECATED, "?%format, use %format" );
					eval_warned++;
				}
				usr_fmt++;
				strncpy( fmt, buf+1, sizeof( fmt ) - 1 );
				break;

			case ']':
				if( rbuf != NULL )                      /* if caller passed a buffer */
					snprintf( rbuf, rbuf_len, fmt, stack[sp-1] );  /* convert to character */
				for( idx = rbuf + strlen( rbuf ) -1; *idx == '0'; idx-- ) {
					if( ! usr_fmt ) {
						if( *idx == '.' ) {
							*idx = EOS;                       /* no fractional amt, cut it out */
						} else {
							idx[1] = EOS;                    /* leave last non 0 there */
						}
					}
				}

				TRACE( 1, "expr: result: %.3f\n", stack[sp-1] );
				return( (int) stack[sp-1] );                /* return top of stack */
				break;

			default:               /* assume its a parameter to push on stack */
				if( *buf == '"' ) {
					sstack[ssp] = strdup( buf+1 );				// push it on
					if( (idx = strchr( sstack[ssp], '"' )) != NULL ) {
						*idx = 0;
					}
					TRACE( 1, "expr: string push: [%d] '%s' buf=(%s)\n", ssp, sstack[ssp], buf );
					ssp++;
				} else {
					stack[sp++] = atof( buf );  /* convert to integer and push on stack */
					TRACE( 1, "expr: push: [%d] %.3f\n", sp-1, stack[sp-1] );
					break;
				}
		}		// switch
	}			// while

	for( ; ssp > 0; ssp-- ) {		// if something pushed on string stack; dump it
		free( sstack[ssp] );
	}

	if( sp == 0 ) {
		stack[sp] = 0;
		sp++;
	}

	// return the top value if the while completes
	if( rbuf != NULL ) {  		// if caller passed a buffer
		TRACE( 1, "expr: exit result: %.3f\n", stack[sp-1] );
		snprintf( rbuf, rbuf_len, "%d", (int) stack[sp-1] );  /* convert to character */
	}

	return (int) stack[sp-1];
}

