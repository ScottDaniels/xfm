/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

/*
	Abstract:	Implement a better way of managing the horrible set of
				bit flags that {X}fm code uses.

	Date:		6 January 2024
	Author: 	E. Scott Daniels
*/

#include <malloc.h>
#include <string.h>

static unsigned char flagBytes[10];

void fmFlagClearAll() {
	memset( flagBytes, 0, sizeof( flagBytes ) );	
}

int fmFlagIsSet( int flag ) {
	int i;
	int f;

	i = flag >> 8;	//index
	f = flag & 0xff;
	return !!(flagBytes[i] & f);
}

void fmFlagSet( int flag ) {
	int i;
	int f;

	i = flag >> 8;
	f = flag & 0xff;
	
	flagBytes[i] |= f;
}

void fmFlagClear( int flag ) {
	int i;
	int f;

	i = flag >> 8;
	f = flag & 0xff;
	
	flagBytes[i] &= ~f;
}

/*
	Returns a snapshot of the current flag settings that can be
	restored later if desired.
*/
void* fmFlagSave( void )  {
	char* flags;
	flags = malloc( sizeof( flagBytes  ) );
	memcpy( flags, flagBytes, sizeof( flagBytes ) );

	return (void *) flags;
}

/*
	Restores the flags using a saved copy. If release is
	true, the memory referecend by saved is freed.
*/
void fmFlagRestore( void* saved, int release ) {
	char* flags;

	if( saved == NULL ) {
		return;
	}

	flags = (char *) saved;
	memcpy( flagBytes, flags, sizeof( flagBytes ) );
	if( release ) {
		free( saved );
	}
}

/*
	Dump the flag values to stderr.
*/
void fmFlagDump( void ) {
	int i;
	int n;

	fprintf( stderr,  "flags:" );
	n = sizeof( flagBytes ) / sizeof( char );
	for( i = 0; i < n; i++ ) {
		fprintf( stderr, " %02x", (unsigned int) flagBytes[i] );
	}

	fprintf( stderr, "\n" );
}
