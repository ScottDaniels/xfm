/*
	Abstract:	Accepts a token and breaks it on the character(s) supplied
				in breakStr. If insertStr is given, the insertStr string is
				inserted at each break point. Driving use, there may be others,
				is to allow for a long URL or pathname to be split automatically.
				As an example, the URL 
					https://some.site.com/?=some+long+string+that+is+obnoxous

				could be split at each + and the command '.sm' added such that
				it would be like the following tokens were in the source:

					https://some.site.com/?=some .sm +long .sm +string .sm +that .sm +is .sm +obnoxous

				The result is that XFM will combine the tokens, without spaces
				(smashed with the .sm command) until the line is filled. The remainder
				of the tokens are placed on the following line, breaking the line
				if again it is not large enough.

	Date:		5 April 2024
	Author:		E. Scott Daniels
*/

#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

void FMcbreak( char* token, char* breakStr, char* insertStr ) {
#	define MAX_BREAK_TOKENS 2048	// excessive, but should cover most insanity on the user's part

	char**	outTokens;			// pointers to tokens to stuff back into the input stream
	char*	tokenBuf;			// spot to build output tokens
	int oIdx = 0;	// index into the outToekens array
	int tbIdx = 0;	// index into token buffer
	int i;

	if( token == NULL ) {
		return;
	}

	if( breakStr == NULL ) {
		AFIpushtoken( fptr->file, token );	// caller expects all results to be back on the input stream
		return;
	}

	outTokens = (char **) malloc( sizeof( char * ) * MAX_BREAK_TOKENS );

	tokenBuf = malloc( sizeof( char ) * 1024 );		// again, excessive token size
	tokenBuf[tbIdx++] = token[0];	// prime (we never split at first character
TRACE( 2, ">>> working on %s\n", token );
	for( i = 1; i < strlen( token ); i++ ) {
		if( strchr( breakStr, token[i] ) != NULL ) {	// this char is a split char
			tokenBuf[tbIdx] = 0;
			outTokens[oIdx++] = strdup( tokenBuf );		// copy for output later
TRACE( 2, ">>> match %c; adding: %s\n", token[i], outTokens[oIdx-1] );
			if( oIdx >= MAX_BREAK_TOKENS ) {
				char msg[256];
				snprintf( msg, sizeof( msg ), "too many tokens in cbreak string" );
				FMmsg( E_OVERRUN, msg );
				return;
			}

			tbIdx = 0;
		}

		tokenBuf[tbIdx++] = token[i];		// add to the current token fragment
	}

	if( tbIdx > 0 ) {						// save the last bits
		tokenBuf[tbIdx] = 0;
		outTokens[oIdx] = strdup( tokenBuf );		// copy for output later
	}
	
TRACE( 2, ">>> pusing %d tokens\n", oIdx );
	for( ; oIdx >= 0; oIdx-- ) {			// output the tokens in reverse since stream is LIFO
		AFIpushtoken( fptr->file, outTokens[oIdx] );	// caller expects all results to be back on the input stream
		if( insertStr != NULL && oIdx > 0 ) {
			AFIpushtoken( fptr->file, insertStr );		// insert the string for all but the last
		}
		free( outTokens[oIdx] );
	}

	free( outTokens );
	free( tokenBuf );
}
