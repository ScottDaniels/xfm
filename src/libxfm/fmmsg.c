/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2016 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

/*
   Mnemonic:	FMmsg
	Abstract:	This module contains the internal messaging function and the
				support for the .mg command.

   Date:		16 November 1988
   Author:		E. Scott Daniels

   Modified:	11 Apr 1994 - To allow for "silent" operation
				03 Apr 1997 - To get file info from AFI
				15 Dec 2005 - To write messages to stderr rather than stdout
				08 Jul 2013 - A bit of cleanup and to fix spacing in message. 
				01 Jan 2016 - Copyright change.
				17 Jul 2016 - Changes for better prototype generation.
				07 May 2020 - Spit line/file on ad hoc messages too
				28 Jun 2020 - Added .mg suport
*/

  /* if the message text begins with a splat (*) then the current line number is also included */

 char *messages[ ] = {
 " FM000: Copyright (c) 1992-2024 E. Scott Daniels. All Rights reserved",
 " FM001: End of file reached for file: ",
 "*FM002: Too many files open. Unable to include: ",
 "*FM003: Unable to open file. File name: ",
 "*FM004: Name on command line is missing. ",
 " FM005: Imbedding file: ",
 "*FM006: Unable to allocate required storage. ",
 " FM007: Lines included from the file = ",
 " FM008: Required parameter(s) have been omitted from command line",
 "*FM009: Unknown command encountered and was ignored",
 "*FM010: Attempt to delete eject failed: not found",
 " FM011: Number of words placed into the document = ",
 "*FM012: Parameter takes margin out of range. ",
 "*FM013: Term length too large. Setting to 1/2 linelength",
 "*FM014: Left margin set less than term size, term ignored",
 "*FM015: Parameter takes line length past the current column width",
 " FM016: Unexpected end of input file",
 "*FM017: Parameter missing, out of range, or unrecognised",
 "*FM018: %%boundingbox statement missing or invalid in file",
 "*FM019: List or table not started. .di/.li/.th command ignored",
 "*FM020: Unrecognized parameter: ",
 "*FM021: Parameter is invalid: ",
 "*FM022: Abort: system error",
 "*FM023: Command or parameter is deprecated:",
 "*FM024: User message",
 "*FM025: Nested table cannot be split across pages automatically",
 "*FM026: Buffer overrun:",
 "*FM026: missing value (= missing?)",
  "  "  };


/*
	This routine writes a message to stderr. If the message
	number is less than 0, then no standard message is written;
	just the ad hoc message in the buffer.
	when a standard message value is passed, the first character
	of the message determines the disposition and is not actually
	printed. Dispositions are:
	'*' or '+'  always print regardless of noisy flag
	' '			print only if noisy

	If the first character of the message is a splat (*)
	the line number and file name are also printed. The line
	number and filename are always printed for ad hoc messages.

	Params:
		msg - Message number of mesage to display.
		buf - Non standard message to display with standard message
*/
extern void FMmsg( int msg, char *buf )
{
	char	wbuf[1024];
	char *ptr;              	/*  pointer at the message */
	char *name = NULL;			/* pointer to file name */
	long line = 0;				/* line number in current file */

	if( fptr != NULL && fptr->file >= 0 )
	{
		AFIstat( fptr->file, AFI_NAME, (void **) &name );  	/* get the file name */
		line = AFIstat( fptr->file, AFI_OPS, NULL ); 		/* get the line number */

		if( !name )
			name = "stdin";
	} else {
		name = "unknown-file";
	}

	if( msg >= 0 && msg < sizeof( messages )  ) {			// standard message number given
		ptr = messages[msg];
		if( ptr[0] != ' ' || fmFlagIsSet( NOISY ) )
		{
			ptr = &ptr[1];              			/* skip the lead character */
			if( ptr[-1] == '*' && fptr != NULL )  	/* print line number too */
				snprintf( wbuf, sizeof( wbuf ), "(%s:%ld) %s: %s", name, line, ptr, buf ? buf : "" );
			else
				snprintf( wbuf, sizeof( wbuf ), "%s: %s: %s", name,  ptr, buf ? buf : "" );

			fprintf( stderr, "%s\n", wbuf );
		}
	} else {
		if( buf != NULL ) {
			fprintf( stderr, "(%s:%ld) %s\n", name, line, buf );
		}
	}
}

/*
	Process the .mg command. The first token is inspected and if it is the
	word abort, then the message is written to std error and we exit bad.
	Otherwise, the message is written to the  tty with an XFM message number
	and we continue.
*/

extern void FMmg( void ) {
	char *buf;			// at next input token to process
	char obuf[4096];	// where we'll build the message
	int len;
	int olen = 0;		// size of string in obuf
	int	tok_num = 0;
	int	abort = 0;

	obuf[0] = 0;
	while( (len = FMgetparm( &buf )) > 0 )		// snarf tokens
	{
		if( tok_num++ == 0 ) {
			abort =  strncmp( buf, "abort", 6 ) == 0;			// allow abort or abort: etc
		}
			
		if( olen + len +1 < sizeof( obuf ) ) {
			strncat( obuf, buf, sizeof( obuf ) - (olen + len + 1) );
			olen += len;
			strncat( obuf, " ", sizeof( obuf ) - (olen + len + 1) );
			olen++;
		}
	}

	FMmsg( E_CMD_MG, obuf );
	if( abort ) {
		exit( 1 );
	}
}
