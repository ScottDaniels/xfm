/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2022 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

/*
****************************************************************************
*
*   Mnemonic:  FMhn
*   Abstract:  This routine sets or resets the header number flag.
*
*   Date:      2 December 1988
*   Author:    E. Scott Daniels
*	Mods:		17 Jul 2016 - Changes for better prototype generation.
*				20 Oct 2022 - complete rewrie to clean up and add alpha support
*
*   .hn {[on|num} | alpha | off | n | f=fmt}
*
*	Setting alpha causes the first of the section number to be a letter starting
*	with 'A'. "on" is left for backwards compatabiliy, but num or alpha should
*	be used going forward. The fmt option allows the user to supply the format
*	string for the header 1 alpha tag. By default it is "Appendix %c: " as this
*	typically will be the general case for alpha sections, but something like
*	"Exhibit %c: " might be desired. We don't automatically add the %c allowing
*	the user to optionally add a colon when desired.
***************************************************************************
*/

extern void FMhn(  void ) {
	char *buf;          /* pointer at the token */
	int len;
	int tookAction = 0;

	while( (len = FMgetparm( &buf )) > 0 ) {	// first of two possible parameters
		tookAction = 1;
		TRACE( 1, "hn: parameter: (%s)\n", buf )
		if( strcmp( buf, "alpha" ) == 0 ) {
			TRACE( 1, "hn: processing alpha\n");
			fmFlagSet( ALPHAHEAD );
			fmFlagSet( PARA_NUM );
			FMset_next_h1( "A" );
			continue;
		}

		if( strncmp( buf, "num", 3 ) == 0 || strcmp( buf, "on" ) == 0 ) { // on for back compatability
			fmFlagClear( ALPHAHEAD );
			fmFlagSet( PARA_NUM );
			continue;
		}

		if( strcmp( buf, "off" ) == 0 ) {
			fmFlagClear( PARA_NUM );
			continue;
		}

		if( strncmp( buf, "f=", 2 ) == 0 ) {
			FMset_alpha1_fmt( &buf[2] );
			TRACE( 1, "added format (%s)\n", &buf[2] );
			continue;
		}

		if( isdigit( *buf ) ) {	// reset if n is given
			pnum[0] = atoi( buf );
			pnum[1] = 0;
			pnum[2] = 0;
			pnum[3] = 0;
			continue;
		}
	}

	if( !tookAction ) {
		fmFlagSet( PARA_NUM );
	}

	TRACE( 1, "hn: finished: buf=(%s)\n", buf )
} 
