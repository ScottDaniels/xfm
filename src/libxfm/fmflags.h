
/*
	Abstract:	Flag constants. Val >> 8 is the index into the byte array
				and val & 0xff can be used then to test that byte for the
				bit flag.

	Date:		6 January 2024
	Author: 	E. Scott Daniels
*/
#ifndef _fmflags_h
#define fmflags_h

#define NOFORMAT		0x100 + 0x01     // do not format input lines 
#define DOUBLESPACE		0x100 + 0x02     // double space flag 
#define TWOCOL			0x100 + 0x04     // indicates two column mode 
#define PARA_NUM		0x100 + 0x08     // indicates we are numbering paragraphs 
#define PAGE_NUM		0x100 + 0x10     // numbering pages when on 
#define JUSTIFY			0x100 + 0x20     // justify text in output file 
#define TOC				0x100 + 0x40     // table of contents enabled 
#define LIST			0x100 + 0x80     // indicates list in progress (last one) 


#define BOX				0x200 + 0x01     // box is in progress 
#define TWOSIDE			0x200 + 0x02     // in twosided mode 
#define SETFONT			0x200 + 0x04     // flush must set font before flushing 
#define PUNSPACE 		0x200 + 0x08     // space after token ending in punctuation 
#define CENTER			0x200 + 0x10     // flush should cen rather than show 
#define RIGHT			0x200 + 0x20     // flush should right rather than show 
#define TRUECHAR 		0x200 + 0x40     // flush will true charpath rather than show 
#define ASIS			0x200 + 0x80     // put input into output (assumes postscript) 

#define SAMEY			0x300 + 0x01    // dont bump up cur y on flush 
#define DIRIGHT			0x300 + 0x02    // right justify def list items 
#define NOISY			0x300 + 0x04    // when set display all info type messages 
#define BORDERS			0x300 + 0x08    // table should have borders 
#define QUOTED			0x300 + 0x10   // inside of back quote marks 
#define DIBUFFER		0x300 + 0x20   // output contains a def list term 
#define OK2JUST			0x300 + 0x40   // ok to justify - set by add tok in tfm 
#define SMASH			0x300 + 0x80	// smash next token to previous one 

#define HYPHEN			0x400 + 0x01 	// hyphinate words if too long (pfm) 
#define NOFONT			0x400 + 0x02	// no <font> things in hfm if doing css work 
#define IDX_SNARF		0x400 + 0x04	// pass words to doc-index snarfing 
#define NEED_STYLE		0x400 + 0x08	// initial style information must be generated 
#define PGNUM_CENTER	0x400 + 0x10	// center the page number 
#define RUNOUT_LINES 	0x400 + 0x20	// lines if running header/footer/pagenum 
#define COLNOTE			0x400 + 0x40	// column note capture in progress 
#define FIG_SECT_NUM	0x400 + 0x80	// use section numbers with figure numbers

#define ROMAN_PN		0x500 + 0x01	// roman numerial page numbers
#define CNPEND			0x500 + 0x02	// column notes imbed is pending
#define PGDCOLS			0x500 + 0x04	// paged columns mode
#define VAR_ASIS		0x500 + 0x08	// as is variable mode
#define WATERMARK		0x500 + 0x10	// watermark is enabled
#define SOFTJUST		0x500 + 0x20	// soft justification (rather than hard edge)
#define ALPHAHEAD		0x500 + 0x40	// header section lables are alpha not numberic
#define COLOUR_PAGE_BG	0x500 + 0x80	// colour page background

#endif
