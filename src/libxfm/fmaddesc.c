/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

/*
****************************************************************************
*
*   Mnemonic:  FMadd_escs
*   Abstract:  This will allocate a new buffer and add escape characters in
*				front of ampersands and escape characters. Mostly this allows
*				an already parsed token, that might have had escaped things
*				to be pushed back onto the read token buffer without
*				trying to "execute" the macros.
*
*   Date:      07 October 2021
*   Author:    E. Scott Daniels
*
***************************************************************************
*/
extern char*  FMadd_escs(  char *buf ) {
	char* nbuf;				// new buffer
	size_t len;
	char*	bp;				// read pointer from buf
	char*	np;				// write pointer in new
		
	len = sizeof( char ) * (strlen( buf ) * 2);		// size of new buffer
	nbuf = (char *) malloc( len );
	if( nbuf == NULL ) {
		fprintf( stderr, "PANIC! cannot allocate copy buffer\n" );
		exit( 1 );
	}

	memset( nbuf, 0, len );
	for( bp = buf, np = nbuf; *bp &&  np < (nbuf+len)-1; bp++ ) {
		if( *bp == ' ' || *bp == '&' || *bp == '^' ) {
			*(np++) = '^';
		}
		*np = *bp;
		np++;
	}

	TRACE( 3, "escaped buffer: (%s)\n", nbuf )
	return nbuf;
}
