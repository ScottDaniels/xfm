/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "fmconst.h"               /* constant definitons */
#include "xfm_const.h"


#include "fmcmds.h"
#include "fmstruct.h"              /* structure definitions */
#include "fmproto.h"

/*
*************************************************************************
*
*   Mnemonic: FMheader
*   Abstract: This routine is responsible for putting a paragraph header
*             into the output buffer based on the information in the
*             header options buffer that is passed in.
*   Parms:    hptr - Pointer to header information we will use
*   Returns:  Nothing.
*   Date:     1 December 1988
*   Author:   E. Scott Daniels
*
*   Modified:	30 Jun 1994 - Conversion to rfm (not much changed)
*				15 Dec 1994 - To seperate skip before/after numbers.
*				03 Jul 2016 - To capture the most recent section number 
*					and header string for .gv command.
*				17 Jul 2016 - Changes for better prototype generation.
**************************************************************************
*/

/*
	Track last and next header/h1 strings for more fancy page and TOC
	numbering options if <section>-<number> type formats are desired.
*/
static char* alpha1Fmt = "Appendix %c: ";	// default level 1 "tag" when in alpha mode
static char* last_snum = NULL;				// last section number generated
static char* last_h1 = NULL;				// last header 1 value
static char* next_h1 = NULL;				// next h1 value
static char* last_stxt = NULL;				// last section text generated

/*
	Return the last "full" header number (e.g. 1.2.3).
*/
extern char* FMget_header_num( ) {
	if( last_snum == NULL ) {
		TRACE(3, "get header: last header number is nil\n" );
		return NULL;
	}

	TRACE(3, "get full header:  returning last full header number (%s)\n", last_snum );
	return strdup( last_snum );
}

/*
	Return the last header 1 section number or alpha value. Nil
	returned if it's not been set.
*/
extern char* FMget_sect_num() {
	if( last_h1 == NULL ) {
		return NULL;
	}

	TRACE(3, "get h1:  returning last h1 value (%s)\n", last_h1 );
	return strdup( last_h1 );
}

/*
	Return the next header 1 section number or alpha value. Nil
	returned if it's not been set.
*/
extern char* FMget_next_h1() {
	if( next_h1 == NULL ) {
		return NULL;
	}

	TRACE(3, "get nxt h1:  returning next h1 value (%s)\n", next_h1 );
	return strdup( next_h1 );
}

extern void FMset_next_h1( char* nxt ) {
	if( next_h1 != NULL ) {
		free( next_h1);
	}

	TRACE(3, "set h1:  set next h1 number (%s)\n", nxt );
	next_h1 = strdup( nxt );
}

extern char* FMget_header_txt( ) {
	if( last_stxt == NULL ) {
		return NULL;
	}

	TRACE(3, "get header text:  getting last section text (%s)\n", last_stxt );
	return strdup( last_stxt );
}

extern void FMset_alpha1_fmt( char* fmt ) {
	if( fmt != NULL ) {
		alpha1Fmt = strdup( fmt );
	}
}

/*
	Creates the section number string.
*/
extern char* FMmk_header_snum( int level ) {
	char	hbuf[100];	// the major header number
	char	wbuf[100];
	char	sbuf[100];
	int i;

	if( last_snum ) {
		free( last_snum );
		last_snum = NULL;
	}

	wbuf[0] = 0;
	hbuf[0] = 0;
	if( fmFlagIsSet( PARA_NUM ) ) {
		if( fmFlagIsSet( ALPHAHEAD ) ) {
			snprintf( hbuf, sizeof(hbuf), "%c", 'A' + (pnum[0]-1) );	// the h1 alpha value
		} else {
			snprintf( hbuf, sizeof(hbuf), "%d", pnum[0] );				// the h1 numeric value
		}

		for( i = 0; i < level; i++ ) {
			if( i ) {
				strcat( wbuf, "." );	// adding something after first level, must add dot
			}

			if( i == 0 && fmFlagIsSet( ALPHAHEAD ) ) {
				if( level == 1 ) {
					snprintf( sbuf, sizeof( sbuf ), alpha1Fmt, 'A' + (pnum[i]-1) );
				} else {
					snprintf( sbuf, sizeof( sbuf ), "%c", 'A' + (pnum[i]-1) );
				}
			} else {
				snprintf( sbuf, sizeof( sbuf ), "%d", pnum[i] );
			}
			strcat( wbuf, sbuf );
		} 
	} 

	if( last_snum != NULL ) {
		free( last_snum );
	}
	last_snum = strdup( wbuf );		// save it

	if( last_h1 != NULL ) {
		free( last_h1 );
	}
	last_h1 = strdup( hbuf );

	hbuf[0]++;
	if( next_h1 != NULL ) {
		free( next_h1 );
	}
	if( fmFlagIsSet( ALPHAHEAD ) ) {
		snprintf( hbuf, sizeof(hbuf), "%c", 'A' + (pnum[0]) );	// the next h1 alpha value
	} else {
		snprintf( hbuf, sizeof(hbuf), "%d", pnum[0]+1 );		// the next h1 numeric value
	}
	TRACE( 3, "header setting next h1: %s\n", hbuf );
	next_h1 = strdup( hbuf );

	return strdup( last_snum );		// return dup for convenience
}

/*
	Save a copy of the last header text.
*/
extern void FMmk_header_stxt( char* txt ) {
	if( last_stxt ) {
		free( last_stxt );
	}

	last_stxt = strdup( txt );
}


