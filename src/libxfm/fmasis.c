/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"

/*
*****************************************************************************
*
*   Mnemonic: FMasis
*   Abstract: This routine is called when the asis flag is set. It
*             assumes that the input in the file is postscript commands
*             and places the text from the input file directly into the
*             output file until the fo coommand is encountered.
*   Parms:    None.
*   Returns:  Nothing.
*   Date:     27 November 1992
*   Author:   E. Scott Daniels
*
*   Modified: 10 Dec 1992 - To use AFI routines for ansi support
*              9 Mar 1993 - To allow .im command to be processed too
*             20 Jul 1994 - To convert to rfm
*              1 Aug 1994 - To write directly from inbuf
*				17 Jul 2016 - Bring decls into the modern world.
*				11 Aug 2024 - pulled back from pfm as there is no need
*							for pfm to have its own version.
*******************************************************************************
*/
extern void FMasis( ) {
	int len;               /* length of string to put in outfile */
	char* cp;

	TRACE( 2, "asis starts\n" );
	while( fmFlagIsSet( ASIS )  && (len = FMread( inbuf )) >= 0 ) {
		for( cp = inbuf; *cp == ' ' || *cp == '\t'; cp++  );		// cp at first non blank or end of string

		if( *cp == CMDSYM ) {
			TRACE( 3, "asis command sym found: %s\n", cp )
			if( *(cp+1) == 'f' && *(cp+2) == 'o' ) {			// format command of somesort
				AFIpushtoken( fptr->file, inbuf );	// push the command for the caller to read
				iptr = optr = 0;              /* return pointing at beginning */
				TRACE( 3, "asis finished\n" );
				break;                       /* get out now - leaving format command for caller */
			} else {
				//if( inbuf[1] == 'i' && inbuf[2] == 'm' ) {  /* imbed file? */
				if( *(cp+1) == 'i' && *(cp+2) == 'm' ) {			// imbed file
					AFIpushtoken( fptr->file, cp+4 );			// push filiename back for the imbed command to read
					cury += textspace + textsize;
					FMimbed( );            			/* then go process it (runs the file */
					inbuf[0] = EOS;        			// reset input buffer
				}
			}
		} else {                      			 /* end if cmd symbol encountered */
			AFIwrite( ofile, inbuf );              /* write it as it came in */
		}
	}  

	iptr = optr = 0;              /* return pointing at beginning */
	optr = 0;                   /* reset pointers */
}

/*
	Like as is, this will expand variables in the otherwise untouched
	input. 
*/
extern void FMvar_asis() {
	char	oBuf[4096];		// output buffer
	char	*tok;			// next token to process
	int		atStart = 1;	// true if first token on a line
	int		oLen = 0;		// length of the output string thus far
	int		len;			// len of current token

	oBuf[0] = 0;
	oLen = 0;
	while( 1 ) {
		if( (len = FMgetparm( &tok )) == 0 ) {	// end of line
			strcat( oBuf, "\n" );
			AFIwrite( ofile, oBuf );
			oLen = 0;
			oBuf[0] = 0;
			atStart = 1;
		} else {
			if( atStart && strcmp( tok, ".fo" ) == 0 )  {
				TRACE( 2, "var as is: format found\n" );
				AFIpushtoken( fptr->file, tok  );
				iptr = optr = 0;
				return;
			}

			if( oLen + len >= 4095 ) {		// we have some limits... silenty enforce them
				AFIwrite( ofile, oBuf );
				oLen = 0;
				oBuf[0] = 0;
			}

			if( oLen > 0 ) {
				strcat( oBuf, " " );
				oLen++;
			}
			strcat( oBuf, tok );
			oLen += len;
			atStart = 0;
		}
	}
}
