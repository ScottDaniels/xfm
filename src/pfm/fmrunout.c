/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"
#include "pfmproto.h"

/*
*****************************************************************************
*
*  Mnemonic: FMrunout
*  Abstract: This routine is responsible for writing the running matter
*            (header, footer and page number) for the current page to the
*            output file.
*  Parms:    page - The current page number
*            shift- TRUE if we should shift the text to the right margin.
*  Returns:  Nothing.
*  Date:     25 October 1992
*  Author:   E. Scott Daniels
*
*  Modified: 10 Dec 1992 - To use AFI routines for ansi compatability
*            21 Feb 1994 - To use rightxy instead of right
*			10 Oct 2007 (sd)  - Added support to anchor head/feet to left col's anchor and not lmar
*			23 Aug 2011 - Added page number centering and user formatted string.
*			23 Dec 2015 - Better management of running header via top gutter.
*			27 Dec 2015 - Fixed bug with header line (twice).
*			17 Jul 2016 - Bring decls into the modern world.
*			11 Aug 2017 - Fix centering so it works properly.
****************************************************************************
*/
extern void FMrunout( int page, int shift )
{
	struct col_blk *cb;		// pointer at the right most column block
	char *cmd = "show";      /* default to show command with header text info */
	int y = 0;               /* spot for writing the footer/page number info */
	int x;                   /* either far left or right depending on shift */
	int hfsize = 10;		// font size
	char *moveto = "moveto"; /* moveto command necessary for show command, but not shifting right */
	char buf[2048];          /* output buffer */
	char	ubuf[1024];		/* if user format string for number, build it here */
	int pn_x;				// x value for centering page nuumber
	int pn_w;				// width for centering page number


	if( rhead == NULL && rfoot == NULL && !fmFlagIsSet( PAGE_NUM ) ) {
		return;               /* nothing to do - get out */
	}

	if( fmFlagIsSet( PGDCOLS ) ) {		// treating colums as pages
		cb = cur_col;							// set based just on the current column
		if( cb ) {
			if( cb != firstcol ) {
				x = cb->lmar;
			} else {
				x = cb->anchor;
			}
		} else {
			return;
		}

		pn_x = x;					// page number centered to colum x and width
		pn_w = cb->width;
	} else {
		//x = firstcol->anchor;						// seed with anchor point, move if there are multiple columns
		for( cb = firstcol; cb != NULL && cb->next != NULL; cb=cb->next ); 		// leaves cb at last column block

		if( cb == NULL )
			return;

		//if( cb != firstcol )
			x = cb->lmar;						// move along as anchor is valid only for first column

		pn_x = 0;					// normal mode, centered page numbers are relative to the physical page
		pn_w = pagew;
	}

	FMsetfont( runfont, runsize );   /* set up the font in the output */
	FMfmt_add( );

	if( shift == TRUE )    					// if we need to shift, use rightxy instead of show and set x to right edge
	{
		cmd = "rightxy";					// rightxy is our internal right justification "show"
		moveto = " ";     					/* moveto not necessary when using right */
		TRACE( 1, "runout: cb->lmar=%d anchor=%d x=%d\n", cb->lmar, firstcol->anchor, x );
		x += cb->width - runrmar;			// align the string on the right at col width of last col, backing off margin distance
		TRACE(1, "runout: shift=true cmd=%s  cury=%d topy=%d x=%d y=%d anchor=%d width=%d runrmar=%d\n", 
				cmd, cury, topy, x, y, firstcol->anchor, cb->width, runrmar );
	} else {
		TRACE(1, "runout: shift=false page=%d cmd=%s  cury=%d topy=%d x=%d y=%d anchor=%d width=%d runlrmar=%d\n", 
				page, cmd, cury, topy, x, y, firstcol->anchor, cb->width, runlmar );
		x += runlmar;
	}
	
	TRACE(1, "runout: header cmd=%s  cury=%d topy=%d x=%d y=%d anchor=%d width=%d rmar=%d\n", cmd, cury, topy, x, y, firstcol->anchor, cb->width, cb->width + cb->lmar );
	if( rhead != NULL )   /* if there is a running header defined */
	{
		y = topy - top_gutter - hfsize;

		if( y > 0 ) {								// only if it will fit
			snprintf( buf, sizeof( buf ), "%d %d %s (%s) %s\n", x, -y, moveto, rhead, cmd );
			TRACE( 2, "runout: writing header command: %s\n", buf );
			AFIwrite( ofile, buf );
		}

		if( fmFlagIsSet( RUNOUT_LINES ) ) {
			if( y > 0 ) {
				snprintf( buf, sizeof( buf ), "%d %d moveto %d %d lineto\n", firstcol->lmar, -(y+4), cb->lmar + cb->width, -(y+4) );
				AFIwrite( ofile, buf );       				/* seperate header from text w/ line */
			}
		}
	}

 	y = boty + 27;  					/* set up the spot for write */

	if( fmFlagIsSet( RUNOUT_LINES ) && ( rfoot != NULL  ||  fmFlagIsSet( PAGE_NUM ) ) ) {  /* draw line to seperate */
		snprintf( buf, sizeof( buf ), " %d %d moveto %d %d lineto stroke\n", firstcol->anchor, -(y-10), cb->lmar + cb->width, -(y-10) );
		AFIwrite( ofile, buf );
	}

	if( rfoot != NULL )   /* if there is a running footer defined */
	{
		snprintf( buf, sizeof( buf ), "%d %d %s (%s) %s\n", x, -y, moveto, rfoot, cmd );
		TRACE( 2, "runout: writing footer command: %s\n", buf );
		AFIwrite( ofile, buf );               /* send out the footer */
		y += 12;
	}

	if( fmFlagIsSet( PAGE_NUM ) ) {
		if( fmFlagIsSet( ROMAN_PN ) ) {		// roman numerals
			snprintf( ubuf, sizeof( ubuf ), "%s", FMi2roman( page ) );		/* format user string, or default if not set */
		} else {
			snprintf( ubuf, sizeof( ubuf ), pgnum_fmt ? pgnum_fmt : "Page %d", page );		/* format user string, or default if not set */
		}

		if( fmFlagIsSet( PGNUM_CENTER ) ) {
			snprintf( buf, sizeof( buf ), "%d %d moveto\n (Times-Roman) (%s) 0 %d [-1]\n %d 1 cent\n", 
				pn_x, -y, ubuf, runsize, pn_w );
		} else {
			snprintf( buf, sizeof( buf ), "%d %d %s (%s) %s\n", x, -y, moveto, ubuf, cmd );
		}

		AFIwrite( ofile, buf );             /* send out the page number */
	} 
}
