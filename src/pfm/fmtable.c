/* All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"
#include "pfmproto.h"

/*
* --------------------------------------------------------------------------
*  Mnemonic: fmtable 
*  Abstract: Start a table
*  Date:     26 Oct 2001 - converted from the hfm flavour 
*  Author:   E. Scott Daniels
*  Mods:
*		10 Apr 2007 - Memory leak cleanup.
*  		10 Oct 2007 : Changed to keep the original col anchor
*		29 Oct 2007 - Adding some trace points.
*		06 Nov 2007 - Corrected table width calculation.
*		21 Mar 2013 - Fixed bug that was preventing borders in a single cell table
*						from being painted correctly.
*		09 Mar 2015 - Added l= and B options to allow line weight to be set to 0
*						and to force edges for tables in tables.
*		22 Dec 2015 - Corrected bug with top border if page eject.
*		24 Dec 2015 - Added r= option for table row command.
*		17 Jul 2016 - Bring decls into the modern world.
*		22 Jun 2019 - Ability to capture border settings for header.
*		04 Oct 2021 - Correct initial padding bug when table in table started
*		29 Dec 2023 - Add background cell colour support; some cleanup.
* 
*  The n option allows user to turn off (no execute) the automatic
*  creation of the first cell. This allows them to call .tr and .cl 
*  straight off in order to supply special values for that cell and row.
*  The B option forces borders (edgges) for tables in tables. l=0 sets
*  table line weight to smallest possible.
* 	.ta [l=line-weight] [B] [b[=0|1]] [n] [a=align] [w=xx] [xi...]
*		[p=padding] [s=cell-space] [class=css-class] [a=align-style]
*		[t=text-colour] [v=valign-style] [w=width-percent]
*		(Some options are html/hfm only and are ignored.)
*  	.et
* 	.cl [c=bgcolour] [s=span-cols] [a=align-type] [h=horiz-pad] [V=vert-pad] (ignored: r= v=  t=)
*	.tr [n] [b=0|1] [c=bgcolour] [a=alignval] [l=lines] [p=pad] [r=space] [v=valignvalue]
*	.th string
*  omitting w= allows browser to scale table. xx is a percentage.
*
* --------------------------------------------------------------------------
*/

// ------ prototypes ------------------------------------------------------
static void endPrevCell( struct table_mgt_blk* t, struct col_blk* );
static void fillCol( struct col_blk *c, int maxy, int padding );
static void resetBGColour( struct table_mgt_blk *t );
static void resetCell( struct col_blk* c, int maxDepth, int padding );
static int spanWidth( struct col_blk* c, int padding );
static void tab_vlines( struct table_mgt_blk *t, int setfree );

// ------ locals -----------------------------------------------------------

/*
	Capture the command and retrun it as a string, The init string
	is used to initialise the command buffer.
*/
static char* capture_cmd( char* init ) {
	char data[4096];
	int	len;
	int	totlen = 0;
	char*	buf;

	*data = 0;
	strcat( data, init );
	totlen = strlen( data );

	while( (len = FMgetparm( &buf )) != 0 ) {
		if( len + totlen + 1 < sizeof( data ) ) {
			strcat( data, buf );
			strcat( data, " " );
		}

		totlen += len + 1;
	}

	return strdup( data );
}

/*
	Do the cleanup needed before moving onto the next cell or
	table row. Things like ending lists, boxes and setting
	the column's max depth. Do NOT call reset BG colour from
	here as that pushes things into the input stream which
	is likley before the current command's parameters are read.
*/
static void endPrevCell( struct table_mgt_blk* t, struct col_blk* c ) {
	if( optr > 0 ) {
		FMflush();
	}

	if( c == NULL ) {
		return;
	}

	FMendlist( FALSE );      // terminate list if in progress
	if( fmFlagIsSet( BOX ) ) {	// must terminate if in progress
		FMbxend( );
	}

	if( c != NULL ) {
		c->maxDepth = cury;		// the furthest down we scribbled in this column
	}

	if( cury > t->maxy ) {
		t->maxy = cury;
	}
}

/*
	BG colour must be turned off at the end of each cell. Which really means at the beginning
	of each new cell, or row, and at the end of table.
*/
static void resetBGColour( struct table_mgt_blk *t ) {
	if( t->bgcolour != NULL ) {		// must turn off previous cell colour
		TRACE(2, "table/cell: turn off background colour (%s)\n", t->bgcolour );
		AFIpushtoken( fptr->file, ".sf h=off - " );	// simulate the setfont command in the input before user text added to cell
		if( t->bgcolour != NULL ) {
			free( t->bgcolour );
		}
		t->bgcolour = NULL;
	}
}

/*
	Given a column block, use its current span setting to compute
	the real width of the column.
*/
static int spanWidth( struct col_blk* c, int padding ) {
	int width = 0;
	int span;
	
	padding = (c->span * padding) - padding;	// total padding across all cells

	for( span = c->span; span > 0 && c != NULL; c = c->next ) {
		span--;
		width += c->width ;
	}

	return width  + padding;
}

// -------------------------------------------------------------------------------

extern void FMtable(  void )
{
	struct table_mgt_blk *t;
	struct	col_blk 	*col;	// tables are just small columns formatted in the normal way
	int do_cell = 1;			/* n option turns off */
	int len;
	char *ptr;
	int border = 0; 		// border size 
	int center = 0;			// if center given on the command, set
	int total_width = 0;	// width of the table: cell sizes + padding

	FMflush( );                         /* terminate the line in progress */

	if( ts_index >= MAX_TABLES ) {
		fprintf( stderr, "abort: too many tables max = %d\n", MAX_TABLES );
		exit( 1 );
	}
	
	t = table_stack[ts_index++] = (struct table_mgt_blk *) malloc( sizeof( struct table_mgt_blk) );

	memset( t, 0, sizeof( struct table_mgt_blk ) );
	t->col_list = firstcol;		/* hold things that need to be restored when done */
	t->cur_col = cur_col; 
	t->topy = cury;				// we need to adjust for padding after reading params
	t->old_topy = topy;
	t->old_linelen = linelen;
	t->lmar = lmar;
	t->hlmar = hlmar;

	t->hpadding = 5;			// horizontal padding
	t->vTopPadding = 0;
	t->vBotPadding = 0;
	t->maxy = cury;
	t->edge_borders = 0;			// default to no edges for table in table
	t->permBGColour = NULL;
	t->rowBGColour = NULL;


	firstcol = NULL;
	cur_col = NULL;

	curcell = 1;        /* tableinfo[0] has cell count */

	while( (len = FMgetparm( &ptr )) != 0 )
		switch( *ptr )
		{
			case 'a': 	// html option ignored
				break;

			case 'B':
				t->edge_borders = 1;;
				break;

			case 'b':   
				if( *(ptr+1) == '=' )
					border = atoi( ptr + 2 );
				else
					border++;
				break;

			case 'C':				// line colour as #rrggbb or 0xrrggbb
				if( *(ptr+1) && *(ptr+2) ) {
					t->line_colour = strdup( ptr+2 );
				}
				break;

			case 'c':
				if( strcmp( ptr, "center" ) == 0 ) {				// table is centered between l and rmar
					center = 1;										// we'll adjust each column start so table is centered
					break;
				} else {
					if( strncmp( ptr, "class=", 6 ) == 0 ) {		// ignore hfm table class
						break;
					} 
				}

				t->permBGColour = strdup( ptr + 2 );		// assume bg colour c=#rrggbb
				TRACE( 3, "whole table bgColour=%s\n", t->permBGColour );
				break;

			case 'h':				// horizontal padding
				t->hpadding = atoi( ptr+2 );
				break;

			case 'l':				// line weight
				t->weight = atoi( ptr+2 );
				break;

			case 'n':
				do_cell = 0;    /* dont auto do a cell */
				break;

			case 'p':								// sets all padding values
				t->vTopPadding = t->vBotPadding = t->hpadding = atoi( ptr + 2 );
				break;
	
			case 's':
				//t->spacing = atoi( ptr+2 );
				break;

			case 't':		// ignore html text colour
				break;

			case 'V':				// vertical padding
				{
					char* tok;
					tok = strchr( ptr+2, ',' );
					if( tok != NULL ) {
						t->vTopPadding = atoi( ptr+2 );
						t->vBotPadding = atoi( tok+1 );
					} else {
						t->vTopPadding = t->vBotPadding = atoi( ptr+2 );
					}
				}
				break;

			case 'v': 			// ignore html valign
				break;

			case 'w':          	// html width percent ignored
				break;

			default:          /* assume next column width definition */
				if( curcell < MAX_CELLS ) {
					col = (struct col_blk *) malloc( sizeof( struct col_blk ) );
					memset( col, 0, sizeof( *col ) );
					col->next = NULL;
					col->maxDepth = cury;
					//col->width = FMgetpts( ptr, len ) - 4;
					col->width = FMgetpts( ptr, len );
					total_width += col->width + t->hpadding;
					//t->border_width += col->width + ( t->hpadding ) + 2;
					t->border_width += col->width;
					if( cur_col ) {
						//col->lmar = cur_col->lmar + cur_col->width + t->hpadding;
						col->lmar = cur_col->lmar + cur_col->width;
						cur_col->next = col;
						cur_col = col;
					} else {
						//col->lmar = lmar + t->hpadding;
						col->lmar = lmar;
						cur_col = firstcol = col;
					}
					curcell++;                                 /* cell counter */
				}
				break;
		}

	if( center ) {
		t->shift = ((linelen - total_width)) / 2;				// amount we need to shift
		t->lmar += t->shift;									// used for horiz lines
		TRACE( 2, "table: centering: linelen=%d wid=%d shift=%d lmar=%d\n", linelen, total_width, t->shift, t->lmar );
		for( cur_col = firstcol; cur_col != NULL; cur_col = cur_col->next ) {
			cur_col->lmar += t->shift;
		}
	}

	firstcol->anchor =  t->col_list->anchor;		/* carry the anchor over for running head/feet */

	t->border = border;
	t->width = total_width;
	cur_col = firstcol;
	if( do_cell ) {
		AFIpushtoken( fptr->file, ".tr : " );		// force the start of a row
	} else {
		for( cur_col = firstcol; cur_col->next; cur_col = cur_col->next );
	}

	lmar = firstcol->lmar + t->hpadding;
	TRACE( 2, "tab_start: idx=%d fclmar=%d hpad=%d vpad=%d,%d lmar=%d edge-border=5d\n", 
		ts_index-1, firstcol->lmar, t->hpadding, t->vTopPadding, t->vBotPadding, lmar )

	topy = cury;										// columns bounce back to here now 
	t->topy = cury;
	if( t->edge_borders ) {	
		snprintf( obuf, IO_BUF_LEN, "%d setlinewidth ", t->weight );
		AFIwrite( ofile, obuf );
		snprintf( obuf, IO_BUF_LEN, "%d %d moveto %d %d rlineto stroke\n", t->lmar, -cury, t->border_width, 0 );
		AFIwrite( ofile, obuf );
	}

	if( trace > 1 ) {
		struct	col_blk 	*cp;
		int	i = 0;
		fprintf( stderr, "table: lmar=%d cury=%d borarder_width=%dp right_edge%dp\n", t->lmar, cury, t->border_width, t->lmar+ t->border_width );
		for( cp = firstcol; cp; cp = cp->next )
			fprintf( stderr, "table: cell=%d lmar=%dp width=%dp\n", i++, cp->lmar, cp->width );
	} 
}

/*
* --------------------------------------------------------------------------
*  Mnemonic: fmth
*  Abstract: save a table header placed into the table now, and if we page eject
*  Date:     02 Nov 2006 - converted from hfm
*  Author:   E. Scott Daniels
* 
*  .th string
* --------------------------------------------------------------------------
*/
extern void FMth( void )
{
	struct table_mgt_blk *t;
	char	*buf;
	int	len; 
	int	totlen = 0;
	char	data[4096];

	if( ts_index <= 0 || (t = table_stack[ts_index-1]) == NULL )
	{
		FMmsg( E_NOT_STARTED, NULL );					// table must be started; write error
		while( (len = FMgetparm( &buf )) != 0 );		/* just dump the record */
		return;
	}
			
	*data = 0;
	while( (len = FMgetparm( &buf )) != 0 )
	{
		if( len + totlen + 2 < sizeof( data ) ) {
			strcat( data, buf );
			strcat( data, " " );
		}

		totlen += len + 2;
	}

	if( t->header ) {					// release previous header and tr info if there
		free( t->header );
		if( t->header_tr ) {
			free( t->header_tr );
			t->header_tr = NULL;
		}
	}
	t->header = strdup( data );
	t->header_border = t->border;			// capture border setting to emulate when adding next header

	TRACE( 2, "tab_header: header in place: %s\n", t->header );
	AFIpushtoken( fptr->file, data );		// put it on to generate the first header
}

/*
* --------------------------------------------------------------------------
*  Mnemonic: fmcell
*  Abstract: Start  the next cell. Parm is a boolean; if true then we expect
*			to read parameters from the input file, otherwise we just provide
*			some comutations for start row.
*
*  Date:     26 Oct 2001 - converted from hfm
*  Author:   E. Scott Daniels
* 
*  .cl [c=bgcolour] [t=fgcolour] [s=span-cols] [a=align-type]
* --------------------------------------------------------------------------
*/
extern void FMcell( ) {
	struct table_mgt_blk *t;
	struct col_blk* c;
	int	span = 1;					/* number of defined columns to span */
	char* bgColour = NULL;
	int len;
	char *ptr;

	if( ts_index <= 0 || (t = table_stack[ts_index-1]) == NULL ) {
		char *b;
		while( (len = FMgetparm( &b )) != 0 );		/* just dump the record */
		return;
	}
	
	endPrevCell( t, cur_col );

	while( (len = FMgetparm( &ptr )) != 0 ) {
		switch( *ptr ) {
			case 'a':	// html style align ignored
				break;

			case 'c':   
				if( strncmp( ptr, "class=", 6 ) == 0 )			/* ignore hfm class */
					break;

				if( bgColour ) {
					free( bgColour );
					bgColour = NULL;
				}
				bgColour = strdup( ptr + 2 );
				break;

			case 'r':		// rowspan ignored
				break;

			case 's':
				span = atoi( ptr+2 );
				break;

			case 't':      /* text colour; really does not make sense to support this */
				break;
		
			case 'v':		// ignore vert alignment
				break;
		
			default: 
				break;
		}
	}

	if( bgColour == NULL ) {						// colour not set; pick up a default if defined
		if( t->rowBGColour != NULL ) {				// default row colour?
			bgColour = strdup( t->rowBGColour );	// reset will free, so this must be a copy
		} else {
			if(  t->permBGColour != NULL ) {		// default table colour?
				bgColour = strdup( t->permBGColour );
			}
		}
	}
	TRACE( 3, "table/cell: bgColour=%s\n", bgColour == NULL ? "none" : bgColour);

	TRACE(2, "table/cell: cury=%d topy=%d textsize=%d textspace=%d font=%s boty=%d maxy=%d span=%d\n", 
		cury, topy, textsize, textspace, curfont, boty, table_stack[ts_index-1]->maxy, span );

	cur_col = cur_col->next;		// move to next, skipping those that previous .cl spanned
	while( cur_col != NULL && cur_col->flags & CF_SKIP ) {
		cur_col = cur_col->next;
	}
	if( cur_col == NULL ) {
		cur_col = firstcol;
	}

	cur_col->span = span;
	if( bgColour != NULL ) {
		cur_col->bgColour = strdup( bgColour );		// if we are filling this cell, save to extend when we complete the row
	}
	cury = topy;
	lmar = cur_col->lmar + t->hpadding;   /* set lmar based on difference calculated */
	hlmar = cur_col->lmar + t->hpadding; /* earlier and next columns left edge */

/*
	if( lilist != NULL )           // if list then reset xpos for next col 
		lilist->xpos = cur_col->lmar + t->hpadding;
*/

	if( span < 1 ) {
		span = 1;
	}

	linelen = 0;			// compute line len: width of this cell, + all we span
	c = cur_col;
	while( span && c != NULL ) {
		linelen += c->width;
		c = c->next;
		span--;
		if( span > 0 ) {
			c->flags |= CF_SKIP;	// skip when adding vertical bars
		}
	}

	linelen -= t->hpadding;
	if( bgColour != NULL ) {
		int width = 0;

		width = spanWidth( cur_col, t->hpadding );
		if( t->bgcolour != NULL ) {
			free( t->bgcolour );
		}
		t->bgcolour = strdup( bgColour );
		char buf[1024];
		snprintf( buf, sizeof(buf), ".sf h=%s l=%dp b=%dp - ", t->bgcolour, width, lmar - t->hpadding );
		TRACE(2, "table/cell assigned t->bgcolour = (%s) [%s]\n", t->bgcolour, buf );
		AFIpushtoken( fptr->file, buf );	// set font with highlighting
	} else {
		resetBGColour( t );
	}

	TRACE(2, "table/cell (end): cury=%d topy=%d maxy=%d bg=%s len=%d\n", 
		cury, topy, table_stack[ts_index-1]->maxy, t->bgcolour ? t->bgcolour : "none", linelen );

	if( bgColour != NULL ) {
		free( bgColour );
	}
}


/*
---------------------------------------------------------------------------
 Mnemonic: fmtr
 Abstract: start a new row in the current table
 Date: 		26 Oct 2001 - converted from hfml stuff
 syntax:   .tr [n] [b=n] [c=bgcolour] [a=alignval] [p=vpadding] [r=reserve] [V=vertPadding] [w=weight] [l=linecount]

			b=n allow borders to be turned on mid table if n == 1.

			w= allows the line weight (drawn between the current row and the 
			   new) to be differnt than the default.

			l= is the number of hoirizontal lines to add before this row.

 			The n option prevents table cell from being called automatically
			(user needs to supply a .cl command prior to the text for the
			frist cell).

			r= (reserve) will force a column break if the desired amount of 

			p=n  allows vertical padding to be changed from the default

			V= vertical padding either n or top,bot

			v=  ignored by pfm.

 Mods:		10 Apr 2007 -- fixed write of border to be in conditional.
---------------------------------------------------------------------------
*/

extern void FMtr( int last )
 {
	int i;
	struct table_mgt_blk *t = NULL;
	struct col_blk*	nextCol = NULL;
	char *ptr = NULL;				/* pointer at parms */
	int len = 0;
	int do_cell = 1;       /* turned off by n option */
	char obuf[2048];
	int col_top = 0;		/* used to calc the depth of each row */
	int	old_cn_space = 0;
	int required = 0;		// points required for the next row; col eject if not enough
	int	tmp_lw = -1;		// temp line weight (l=)
	int lcount = 1;			// number of horizontal separating lines
	int border = -1;		// update border starting after we paint next top border
	int topPadding;			// holds override values when supplied
	int botPadding;
	char*	cmd_str;		// the command string if we must capture it

	if( ts_index <= 0 || (t = table_stack[ts_index-1]) == NULL ) {
		char *b;
		while( (len = FMgetparm( &b )) != 0 );			// no table, ditch parms and scoot
		return;
	}

	if( t->rowBGColour != NULL ) {
		free( t->rowBGColour );
		t->rowBGColour = NULL;
	}

	if( t->header != NULL && t->header_tr == NULL ) {		// first tr after header, must capture
		cmd_str = capture_cmd( ".tr " );					// pull all parameters from input as a string
		t->header_tr = cmd_str;
 		AFIpushtoken( fptr->file, cmd_str );				// push the command string and we'll be recalled
		return;
	}

	topPadding = t->vTopPadding;		// default to current padding; p= overrides
	botPadding = t->vBotPadding;

	if( cur_col != NULL ) {
		cur_col->maxDepth = cury;		// final capture for the last in progress colummn
	}
	if( cur_col == firstcol ) {			/* we've not seen a .cl command (one col table) */
		t->maxy = cury;					/* force it to be set */
	}
	col_top = cury;

	TRACE(2, "table/tr: cury=%d textsize=%d textspace=%d font=%s boty=%d topy=%d maxy=%d\n", 
		cury, textsize, textspace, curfont, boty, topy, table_stack[ts_index-1]->maxy );

	nextCol = cur_col;
	while( nextCol != NULL ) {		// do the rest if cur-col is not the last (.tr given early)
		endPrevCell( t, nextCol );
		cur_col = nextCol;				// must leave cur_col at last for the next .cl
		nextCol = cur_col->next;
	}
	
	t->tot_row_depth += t->maxy - col_top;		/* add in depth of this column */
	t->nrows++;
	t->ave_row_depth = t->tot_row_depth / t->nrows; 	/* average depth of a row to predict end of page */

   while( (len = FMgetparm( &ptr )) != 0 ) {
     switch( *ptr ) 
      {
       case 'a':		// ignore HTML align
         break;

		case 'b':									// allow b=0 on table, but turning it on at any  row with b=1
			if( *(ptr+1) == '=' ) {
				border = atoi( ptr+2 );
			}
			break;
	
       case 'c':
		if( strncmp( ptr, "class=", 6 ) == 0 )			/* ignore hfm class */
			break;

		t->rowBGColour = strdup( ptr+2 );
         break;

		case 'l':
			if( *(ptr+1) == '=' ) {
				lcount = atoi( ptr+2 );
			}
			break;

       case 'n':									// don't set up the first cell by default
         do_cell = 0;
         break;

		case 'p':
			{
				char* tok;
				tok = strchr( ptr+2, ',' );
				if( tok != NULL ) {
					topPadding = atoi( ptr+2 );
					botPadding = atoi( tok+1 );
				} else {
					topPadding = botPadding = atoi( ptr+2 );
				}
			}
			break;

		case 'r':									// space required for this row
			required = FMgetpts( ptr + 2, len-2 );
			break;

       case 'v':		// ignore html valign
         break;

		case 'w':
			if( *(ptr+1) == '=' ) {
				tmp_lw = atoi( ptr+2 );
			}
			break;


       default: 								// ignore anything unrecognised
         break;
      }
    }

	if( t->nrows > 1 ) {
		cury = t->maxy + 6 + botPadding;		// additional 6 to get below decenders
	}

	snprintf( obuf, IO_BUF_LEN, "%d setlinewidth ", t->weight );
	AFIwrite( ofile, obuf );
	tab_vlines( t, last );					// add vert lines just for this row 

	if( border >= 0 ) {						// reset after we paint verts for previous row
		t->border = border;
		if( tmp_lw < 0 ) {					// force change if user didn't set explicitly
			tmp_lw = t->weight;
		}
	}

/*
	if( last && !t->edge_borders ) {
		return;
	}
*/

	if( t->border || tmp_lw >= 0 ) {							// add a top line if borders, or a temp line width was given
		int line_y = cury;

		TRACE( 2, "table/tr-border: cury=%d lcount=%d border=%d tlw=%d\n", cury, lcount, border, tmp_lw );
		if( lcount && tmp_lw >= 0 ) {							// dont need if no line generated
			snprintf( obuf, IO_BUF_LEN, "%d setlinewidth ", tmp_lw );		// need a second adjustment
			AFIwrite( ofile, obuf );
		}

		if( t->nrows > 1 ) {									// row division line
			for( i = 0; i < lcount; i++ ) {
				snprintf( obuf, IO_BUF_LEN, "%d %d moveto %d %d rlineto stroke\n", t->lmar, -line_y, t->border_width, 0 );
				AFIwrite( ofile, obuf );								// bottom line for the previous row
	
				if( i < lcount - 1 ) {
					line_y += 2;
				}
			}
		}
	}

	if( last ) {
		return;
	}

	TRACE( 1, "table/tr: required=%d cn_space=%d ard=%d cury=%d boty=%d remain=%d pad=%d,%d\n", 
		required, cn_space, t->ave_row_depth, cury, boty, boty-cury, topPadding, botPadding );
	
	// ??? Do we need to prevent eject if this is the last one?
	if( (required + cury) > (boty-8)  ||  
		cn_space + cury + t->ave_row_depth + textsize + textspace >= (boty-8) ) {	// extra 8pts to have room for bottom line
		if( ts_index > 1 ) {					// nested tables cannot be restarted
			FMmsg( E_SPLIT_TAB, NULL );
		} else {
			FMpause_table();					// pause so we can eject to the next real column which might be a page eject
			AFIpushtoken( fptr->file, ".rt" ); 	// must restart table;  push first so that col notes go before if needed
			old_cn_space = cn_space;			// eject will reset if set
			PFMceject(  );						// move to the top of the new col/eject page
			if( old_cn_space > 0 ) {			// if col note, must set it up and return so it is processes before new col is started
				t->maxy = topy;
				return;							// allow the col notes commands to play out first
			}

			topy = t->old_topy;
			cury = topy;
			t->maxy = t->topy;					// reset the mexy for the next col/page
		}
	}

	// this must be AFTER table split so that the push token is in the right order
	// yes, the reset bg colour comes AFTER this so that its push token is on the top of the stack
	if( do_cell && ! last) {					// we need to 'set up' the first column as a convenience
		AFIpushtoken( fptr->file, ".cl : " );
	}
	resetBGColour( t );

	t->topy = cury;
	if( !last ) {
		TRACE( 1, "table/tr: add topPadding %d + %d = %d | curcol=%p\n", cury, topPadding, cury+topPadding, cur_col );
		cury += topPadding;					// add follow padding only if writing lines
	}

	topy = cury;			/* columns need to bounce back to here */
 }

/*
	Given a column block, fill the portion of the column from the
	last write (max depth) to the maxy given as a parameter with 
	the BG colour saved in the column.
*/
static void fillCol( struct col_blk *c, int maxy, int padding ) {
	char obuf[2048];
	if( c->bgColour == NULL ) {
		return;
	}

	if( c->maxDepth > 0 && c->maxDepth < maxy ) {
		int height = maxy - c->maxDepth;
		char* colourStr;
		int width = 0;

		width = spanWidth( c, padding ) + (c->span * padding);	// must add in to account for hidden padding in postscript function
		colourStr = FMparsecolour( c->bgColour ), 
		snprintf( obuf, sizeof(obuf), "%s %d -%d %d %d fillCell", colourStr, c->lmar, maxy+5, width, height );
		AFIwrite( ofile, obuf );
		free( colourStr );
	}
}

/*
	When the row is finished we must fill the cell from the last text to the
	max depth across the row, and then we must reset the cell's colours.
	Padding is the additional horizontal padding added.
*/
static void resetCell( struct col_blk *c, int maxDepth, int padding ) {
	if( c->bgColour == NULL ) {	// unneeded if no bg colour is set
		return;
	}

	fillCol( c, maxDepth, padding );
	c->maxDepth = 0;
	free( c->bgColour );
	c->bgColour = NULL;
}

/*
	Called after all cells in a row have been written to. This function
	adds vertical lines for a single row in the table. Setfree is true
	when we help out by freeing the column blocks. There are other column
	reset things that are also handled here.
*/
static void tab_vlines( struct table_mgt_blk *t, int setfree ) {
	struct col_blk *c;		// current column for work
	int skipVL = 0;
	struct col_blk* next;

	snprintf( obuf, IO_BUF_LEN, "%d setlinewidth ", t->weight );
	AFIwrite( ofile, obuf );
	skipVL = !t->edge_borders;		// if outside borders are off, skip cell 0 for (left) vert line
	for( c =  firstcol; c; c = next ) {
		next = c->next;				// because we might free c!
		resetCell( c, t->maxy, 3 );	// 3 is a magic number; the postscript highlight function adds 3, so we must

		if( !skipVL ) {											// ok to write (left) vert line for this cell
			if( (c->flags & CF_SKIP) == 0  ) {					// if not a spanned (skipped) column
				snprintf( obuf, IO_BUF_LEN, "%d %d moveto %d %d lineto stroke\n", c->lmar, -t->topy, c->lmar, -cury );
				AFIwrite( ofile, obuf );
			}
		}
	
		skipVL = t->border == 0;	// skip all vert lines if borders are off, so keep set

		if( setfree ) {			// cleaning up, col is deleted
			free( c );
		} else {				// otherwise we reset flags and counts as needed
			c->flags &= ~CF_SKIP;
			c->span = 1;
		}
	}

	if( t->edge_borders  )	// need right edge border
	{
		int x;

		TRACE( 2, "table/vlines: topy=%d cury=%d maxy=%d lw=%d\n", t->topy, cury, t->maxy, t->border );
		snprintf( obuf, IO_BUF_LEN, "%d setlinewidth ", t->weight );
		AFIwrite( ofile, obuf );

		x = t->lmar + t->border_width; 
		snprintf( obuf, IO_BUF_LEN, "%d %d moveto %d %d lineto stroke\n", x, -t->topy, x, -cury );
		AFIwrite( ofile, obuf );
	}
}

/*
	Pause the table, restoring columns etc to what they were. Allows column notes to be placed at the
	end of the page when a table spans a page boundary. We assume table row has been the source of 
	this and has cleaned up the last row, so no need to call.
*/
extern void FMpause_table( void ) {
	struct	table_mgt_blk *t;

	if( ts_index <= 0 || ! (t = table_stack[ts_index-1]) )
		return;

	TRACE( 1, "pause_table: cury=%d rows=%d  total_depth=%d  ave_depth=%d\n", cury, t->nrows, t->tot_row_depth, t->ave_row_depth );

	t->paused_list = firstcol;				// hold the current list
	t->paused_col = firstcol;

	lmar = t->lmar;							// restore settings for the column note, or whatever needs regular setup
	hlmar = t->hlmar;
	linelen = t->old_linelen;
	topy = t->old_topy;
	cur_col = t->cur_col;
	firstcol = t->col_list;
}

/*
	Restart a paused table
*/
extern void FMrestart_table( void ) {
	struct	table_mgt_blk *t;

	if( ts_index <= 0 || ! (t = table_stack[ts_index-1]) ) {
		return;
	}

	TRACE( 2, "restart-tab: idx=%d cury=%d head=%s border=%s\n", ts_index, cury, t->header ? "yes" : "no", t->border ? "yes" : "no" );
	if( t->paused_list == NULL ) {
		fprintf( stderr, "abort: internal mishap restarting table; no paused list\n" );
		exit( 1 );
	}

	firstcol = t->paused_list;
	cur_col = firstcol;
	lmar = cur_col->lmar + t->hpadding;
	firstcol->anchor =  t->paused_list->anchor;			// cary the anchor over for running head/feet
	t->maxy = cury;

	for( ; cur_col->next != NULL; cur_col = cur_col->next ); 	// must leave cur col at the last col
	if( t->header ) {										// new header on resume
		t->border = t->header_border;					// simulate border state from start of table
		TRACE( 2, "restart-tab: header: %s\n", t->header );
		TRACE( 2, "restart-tab: header first tr: %s\n", t->header_tr );

														// push in reverse order...
		AFIpushtoken( fptr->file, t->header_tr );		// push the tr command that followed the header
		AFIpushtoken( fptr->file, t->header );			/* fmtr calls get parm; prevent eating things */
		AFIpushtoken( fptr->file, ".cl : " );
		return;
	}

	AFIpushtoken( fptr->file, ".cl : " );				// no header, just force a new column
}

extern void FMendtable( void )
{
	struct	table_mgt_blk *t;

	if( ts_index <= 0 || ! (t = table_stack[ts_index-1]) )
		return;

	TRACE( 1, "end_table: cury=%d rows=%d  total_depth=%d  ave_depth=%d\n", cury, t->nrows, t->tot_row_depth, t->ave_row_depth );

	AFIpushtoken( fptr->file, ":" );			/* fmtr calls get parm; prevent eating things */
	FMtr( 1 );		// flush out previous row adding bottm line and filling colours if needed; will free columns

	TRACE( 2, "tab_end: reset first col from %p to %p\n", firstcol, t->col_list );
	TRACE( 2, "tab_end: reset cur_col from %p to %p\n", cur_col, t->cur_col );
	resetBGColour( t );
	cur_col = t->cur_col;
	lmar = t->lmar - t->shift;			// adjust for shift if table was centered
	hlmar = t->hlmar;
	linelen = t->old_linelen;
	firstcol = t->col_list;

#ifdef KEEP
/* setting the temp top is up to the user as the table might be in a left column and the next 
column needs to go to the top of the physical page.  If this is a page wide table, then 
the user must setup multiple columns after .et and set the temp top.  we cannot assume this 
to be the case
*/
	if( rtopy == 0 )			/* allow for multiple columns under the table */
		rtopy = t->old_topy;

	topy = cury;
#endif

	topy = t->old_topy;
	if( t->header )
		free( t->header );
	if( t->line_colour ) {
		free( t->line_colour );
	}
	if( t->bgcolour ) {
		free( t->bgcolour );
	}
	if( t->permBGColour ) {
		free( t->permBGColour );
	}

	memset( t, 0, sizeof( struct table_mgt_blk ) );	// can't hurt
	free( t );
	table_stack[ts_index-1] = NULL;
	if( ts_index > 0 )
		ts_index--;			/* keep it from going neg */
	if( ts_index )
		topy = table_stack[ts_index-1]->topy;
}

/*
	With the potential for cell background colour, we must use fillCol to fill
	the cell when adding white space via the .sp command. This does just
	that. Depth is the 'bottom y' value that we fill to.

	We assume that it is safe to set the max depth in the column to the current y.

	Returns true if there is a table in progress and we did something.
*/
extern int FMtableSpace( int depth ) {
	struct table_mgt_blk* t;

	if( ts_index <= 0 || ! (t = table_stack[ts_index-1]) )
		return 0;

	cur_col->maxDepth = cury;
	fillCol( cur_col, depth, 3 );	// 3 is the magic amount that the underly postscript adds
	return 1;
}
