/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2022 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

#include <sys/types.h>
#include <stdio.h>     
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>
#include <errno.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"
#include "pfmproto.h"

/*
*****************************************************************************
*
*  Mnemonic:	FMcolnotes
*  Abstract:	Functions to support the .cn (column notes) command and the inclusion
*				of registered notes at the end of the column or at the end of the text.
*
*				Column notes now forces an early page flush if the next atbot note
*				would not have room. To that end, it might be wise to add the
*				note in the source above the reference:
*
*					.cn start atbot Times-roman 8 .5i
*						A small round looking thing that might go unnoticed.
*					.cn end
*					He had in his hand a warble-goob &note which he hoped to sneak in.
*
*				This shold provide better "protection" against accidentally flushing
*				the line with the reference before the .cn command is processed.
*
*  Date:     	10 Mar 2013
*  Author:   	E. Scott Daniels
*
*  Mods:	04 Jan 2016 - Force a break at the end of the text.
*			17 Mar 2018 - Correct compiler printf warnings
*			05 Jul 2018 - Add ability to set indent/line len and to turn block 
*				centering off as well.
*			12 Feb 2022 - some cleanup of formatting and comments. Also now
*				accepts 'atend' as a synonym to 'atclose'. All positional parameters
*				can be supplied with x= style options. Reorganised parsing for better
* 				readability. Default font is Times, font size is 8p and default
*				reserved space is .5i. For "atbot" notes, symbol is assumed to be
*				from the zap font, and for "atend/atclose" symbol can be a string
*				e.g. Kroek82. If symbol is not provided, a sequence number is used.
*			16 Oct 2022 - Fix bug when note hits too close to the end of a page.
*
# Syntax:
* .cn start {atclose | atbot} [option(s)] [font fontsize space]
*		options may be any of the following in any order:
*			f=<font-name>
*			i=<indent-value>{p|i}
*			p=<font-points>
*			l=<linelen-value>{p|i}
*			r=<reserved-space>{p|i}
*			s={<symbol>}
*
* .cn end
*
*	For notes at column bottom, the first character of the symbol is used. For end
#	notes, the symbol is treated as a string allowing things like [Kroker04].
* .cn end
* .cn show
*
*	Examples:
*		.cn start atbot s=none Times-Roman 8p 1i	.** no number/symbol added
*		.cn start atbot s=* Times-Roman 8p 1i
*		.cn start atbot  Times-Roman 8p 1i
*		.cn start atbot  i=.5i l=&line_len Times-Roman 8p 1i
****************************************************************************
*/

static FILE	*bfile = NULL;		// bottom of page/column file
static FILE *efile = NULL;		// end file
static FILE *target = NULL;		// last file written to (needed to cleanup for .cn end)
static int	eid = 1;			/* note numbers */
static int  bid = 1;
static char bfname[1024];
static char efname[1024];


static void cnstart( ) {
	char	*buf;
	char	ocmd[1024];				// spot to build a command in
	int		len;
	char	*symbol = NULL;
	int		*target_id = NULL;
	int		indent = -1;			// i=value given
	int		llen = -1;				// l=value given
	int		need_init = 0;			// true if we need an initialisation string into target
	char	*font = NULL;
	int		font_size = 8;
	int		en_width = 25;			// end note di width
	int		reserve = 36;			// amount of vertival space to reserve
	int		pparm = 0;				// current positional parameter

	font = strdup( "Times-Roman" );
	snprintf( ocmd, sizeof(ocmd), ".cn start" );
	while( (len = FMgetparm( &buf )) > 0 ) { 	 // while at* keyword or ?=value 
		if( len + strlen( ocmd ) + 1 < sizeof( ocmd ) ) {	// build the command in case we have to flush the page
			strcat( ocmd, " " );
			strcat( ocmd, buf );
		}

		if( strcmp( buf, "atclose" ) == 0 || strcmp( buf, "atend" ) == 0 ) {	// safe to open end file now
			target_id = &eid;
			if( !(target = efile) ) {			// yes this is an assignment! file not open
				snprintf( efname, sizeof( efname ), "%d.ecnfile", getpid() );
				TRACE( 2, "colnotes: opening efile: %s\n", efname );
				target = efile = fopen( efname, "w" );
				if( target == NULL ) {
					fprintf( stderr, "colnote: unable to open tmp file: %s\n", efname );
					exit( 1 );
				}
				need_init = 2;	// init and start a di list
			}
		} else {
			if( strcmp( buf, "atbot" ) == 0 ) {
				target = bfile;		// assign; if nil, we'll open after potentially flushing the page, but later
			} else {
				if( *buf && buf[1] == '=' ) {				// ?=parm is given
					switch( *buf ) {	
						case 's':							// symbol given rather than using one we'd generate
							if( strcmp( buf, "s=none" ) != 0 ) {
								symbol = strdup( buf+2 );
							}
							break;

						case 'f':			// font name
							font = strdup( buf+2 );
							break;

						case 'i':							// indent given
							indent = FMgetpts( buf+2, 0 );
							break;

						case 'l':							// line len given
							llen = FMgetpts( buf+2, 0 );
							break;

						case 'p':			// font size (points)
							font_size = atoi( buf+2 );
							break;

						case 'r':			// amount of reserved vert space for the note
							reserve = FMgetpts( buf+2, 0 );
							break;

						case 'w':			// end note width
							en_width = FMgetpts( buf+2, 0 );
							break;
					}
				} else {
					switch( pparm ) {
						case 0:		// font name
							font = strdup( buf );
							break;

						case 1:		// font size
							font_size = atoi( buf );
							break;

						case 2:
							reserve = FMgetpts( buf, len );

						default:		break;		/* ignore rest */
					}

					pparm++;
				}

			}
		}	
	}	// end while params

	if( (cury + cn_space + reserve + 2) > boty ) {		// must force a break now; no hope of adding this to current page
		TRACE( 1, "colnotes: early flush forced: %d + %d > %d\n", cury, cn_space+reserve+2, boty);
		if( strlen( ocmd ) < sizeof(ocmd) - 1 ) {
			strcat( ocmd, " : " );
		}
 		AFIpushtoken( fptr->file, ocmd ); 			// push the original command back to execute after flush
		FMpflush();
		return;
	}

	if( target == NULL ) {	// target not end notes, or open col notes file; open col notes now
		target_id = &bid;
		if( !(target = bfile) ) {		// yes this is an assignment! -- file not open
			snprintf( bfname, sizeof( bfname ), "%d.bcnfile", getpid() );
			TRACE( 2, "colnotes: opening bfile: %s\n", bfname );
			target = bfile = fopen( bfname, "w" );
			if( target == NULL ) {
				fprintf( stderr, "unable to open tmp file: %s: %s\n", bfname, strerror(errno) );
				exit( 1 );
			}
			need_init = 1;
		}
	}

	if( need_init ) {				// we opened the file, so must write a complete initialisation command string
		fprintf( target, ".fo\n.pu\n.bc end\n.ju off\n.sp .3\n.lw 0\n" ); 		// every time
		fprintf( target, ".sf %s .st %d\n", font, font_size );
		if( need_init > 1  &&  font ) {
			fprintf( target, ".bd %dp %s\n", en_width, font );	// end notes are a di list
		}

		if( llen > 0 ) {									// if options given
			fprintf( target, ".ll %dp\n", llen );
		}
		if( indent > 0 ) {
			fprintf( target, ".in %dp\n", indent );
		}

		if( need_init == 1 ) {								// no lines on end notes
//		.ln [l=[+|-]lmar] [r=[+|-]rmar] [a=pts]
			fprintf( target, ".hl\n" );						// finally draw the line
		}
	} else {
fflush(stderr);
		fprintf( target, ".sp .5\n" );						// next entry, add space
	}
	if( font ) {
		free( font );
	}

	TRACE( 1, "colnotes: now reserving %dp at end of col for note(s)\n", cn_space );

	if( target_id == &eid ) {				/* end notes have [%d] rather than super script format */
		if( symbol != NULL ) {
			fprintf( target, ".di ^%s\n", symbol );
		} else {
			fprintf( target, ".di ^[%d]\n", (*target_id)++ );
		}
	} else {
		if( cn_space == 0 ) {
			cn_space = 10;			// small gutter
		}
		cn_space += reserve;
		if( symbol ) {
			fprintf( target, ".ll -.2i .in +.15i .tf superscript ZapfDingbats 2/3 %c\n", *symbol );		/* writes in default font */
		} else {
			fprintf( target, ".in +.15i .ll -.2i .tf superscript 2/3 %d\n", bid++ );		/* writes in default font */
		}
	}

	if( symbol != NULL ) {
		free( symbol );
	}

	/* continue to read input until we find end or the end command -- write stuff to the target file */
	while( FMgettok( &buf ) ) {
		if( strcmp( buf, ".cn" ) == 0 ) {
			FMgetparm( &buf );				/* we'll assume its .cn stop -- dont allow nested */
			break;
		}
		fprintf( target, "%s ", buf );
	}

	if( target_id == &eid ) {	// into the end notes file
		fprintf( target, "\n" );
	} else {
		fprintf( target, "\n.br .in -.15i .ll +.2i\n" );
	}

	TRACE( 2, "colnotes: end notes cn_space=%d\n", cn_space );
}

/*
	Put the closing bits to the last file we wrote to and reset target.
	If it was the bfile, that is still open, so just set target to nil.
*/
static void cnend( ) {
	if( target != NULL ) {
		fprintf( target, ".po\n" );
	}

	target = NULL;
}

/*
	show the col notes by closing the file and pushing an imbed 
	command onto the stack.  the last command in the file is a 
	.cn command to unlink the file (we'll leave droppings in the file
	system if the user causes us to crash, but thats low risk).

	returns true if something was pushed -- needed for the end;
*/
extern int FMcolnotes_show( int end ) {
	char	buf[2048];		// must be larger than filename buffers
	FILE	*target = NULL;
	//int		dref_bfile = 0;
	char	*fname = NULL;
	int		status = 0;
	char	*end_cmd = "";

	if( bfile != NULL )		// target is always the b file if it's there
	{
		target = bfile;
		//dref_bfile = 1;
		bfile = NULL;
		fname = bfname;
	}
	if( end )				/* end of doc (atclose) */
	{
		FMflush( );
		if( target == NULL ) {			// end and no bfile; try end file now
			target = efile;
			efile = NULL;
			fname = efname;
		}

		end_cmd = ".ed";
	}
	
	TRACE( 1, "colnotes: showing at: %s cury=%d boty=%d cn_space=%d page=%d target=%p\n", 
		end ? "end of doc" : "bottom of col", cury, boty, cn_space, page, target );

	if( target == NULL )
		return 0;

	if( cn_space > 0 )
		if( boty - cn_space > cury )
			cury = boty - cn_space;
	TRACE( 1, "colnotes: after adjustment cury=%d\n", cury );

	cn_space = 0;


	fprintf( target, ".br\n.po\n" );					// add the ending commands to the file (force flush and pop)

	if( ! end )
		fprintf( target, ".cb\n" )	;					// new col only if not at end; causes extra page eject if at end
	if( fmFlagIsSet( JUSTIFY ) ) {
		if( fmFlagIsSet( SOFTJUST ) ) {
			fprintf( target, ".ju soft\n" );				// justify back to soft mode
		} else {
			fprintf( target, ".ju on\n" );					// justify back on if needed
		}
	}
	fprintf( target, ".cn unlink %s\n%s\n", fname, end_cmd );

	if( fmFlagIsSet( NOFORMAT ) ) {
		fprintf( target, ".nf\n" );
		fmFlagSet( CNPEND );							// format function signal to yield
		TRACE( 1, "colnotes: show: setting nf\n" );
	}

	if( target != NULL ) {	// once shown we are done with the file; close it and drop refs
		if( target == bfile ) {
			bfile = NULL;		// cant use a closed file
		} else {
			efile = NULL;
		}
		fclose( target );
		target = NULL;
	}

	snprintf( buf, sizeof( buf ), "\n.im %s", fname );				/* must have a guarding newline as lead (prevent accidents with .sp without optional parm etc.) */
 	status = AFIpushtoken( fptr->file, buf );  						/* push to imbed the file */
	TRACE( 2, "colnotes: pushing: stat=%d (%s)\n", status, buf );
	*fname = 0;

	return 1;
}

extern void FMcolnotes( )
{
	char	*buf;

	if( FMgetparm( &buf ) > 0 )
	{
		if( strcmp( buf, "start" ) == 0 )
			cnstart();
		else
		if( strcmp( buf, "showend" ) == 0 )
			FMcolnotes_show( 1 );
		else
		if( strcmp( buf, "show" ) == 0 )
			FMcolnotes_show( 0 );
		else
		if( strcmp( buf, "end" ) == 0 )
			cnend();
		else
		if( strcmp( buf, "unlink" ) == 0 )
		{
			if( FMgetparm( &buf ) > 0 )		/* unlink the used file we put on the imbed command */
				unlink( buf );	
		}
	}
}
