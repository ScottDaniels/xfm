/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license.

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:

		1. Redistributions of source code must retain the above copyright notice, this list of
		conditions and the following disclaimer.

		2. Redistributions in binary form must reproduce the above copyright notice, this list
		of conditions and the following disclaimer in the documentation and/or other materials
		provided with the distribution.

	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ctype.h>
#include <string.h>
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmconst.h"
#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"
#include "pfmproto.h"
/*
**********************************************************************************************************

   Mnemonic: FMfigure
   Abstract: This routine places a blank line and then the word "figure"
             and figure number into the file. The line following this
             line will contain the user input statement if entered on the
             command line. This routine should later be modified to contain
             the logic if a figure table will ever be generated. Figure
             numbers are the section number followed by a sequential number.

				If 'nosect' is given on the command, then the command is
				used just to set the flags which control whether or not
				section style numbering is used when paragraph numbering is
				turned on.
   Parms:    None.
   Returns:  Nothing.
   Date:     9 December 1988
   Author:   E. Scott Daniels


				Two styles:
				.fg nosect

				.fg [s=size] [f=font] [v=var_name] [num=number] [t=type] [x=<string>] <text>
					type is either table or figure, default is figure.
					x=<string> allows "flags" such as x=center to be set. Supported
					flags are:
						x=bold    == bold the Figure xxx:  or Table xxx:  portion
						x=center  == center the figure string
						x=reset	  == reset counters to 1

				<text> may contain commands, but commands need to be contained on one line
				and commands such as .tf will require colon (:) terminators. Further, a
				macro containing a command which needs a colon terminator will need to be
				escaped in the <text> portion, e.g.:

					.fg x=center Illustration of ^&bold(escaping) a macro in the text.

   Modified:	30 Oct 1992 - To reduce figure text size by two from current size
				26 May 1993 - To reduce space between figure text lines.
				22 Feb 1994 - To ensure text font is set properly
			    13 Oct 2001 - To setup for fonts using the font blocks
				04 Mar 2014 - To allow for preallocation of figure numbers and
					to support their use on the .fg command
				17 Jul 2016 - Bring decls into the modern world.
				11 Aug 2017 - Add size (s=), font (f=), and nosect parm support.
				20 Jun 2019	- Add x= support
				15 Sep 2021 - Add ability to embed commands (e.g. .tf) in the fig command; add x=bold
				11 Feb 2022 - Fix flag bug
**********************************************************************************************************
*/

typedef struct push_buf {
	char*	buf;
	int		nalloc;
	int		used;
	int		exsize;		// expansion size
} Push_buf_t;

extern Push_buf_t* mk_pb( int est_size ) {
	Push_buf_t* pb;

	pb = malloc( sizeof( *pb ) );
	if( pb == NULL ) {
		fprintf( stderr, "panic: cannot make push buf; no mem\n" );
		exit( 2 );
	}

	if( est_size > 1024 ) {
		pb->exsize = est_size;
	} else {
		pb->exsize = 1024;
	}

	pb->nalloc = est_size * 2;
	pb->used = 0;
	pb->buf = malloc( pb->nalloc );
	if( pb == NULL ) {
		fprintf( stderr, "panic: cannot make push buf; no mem for buf sized: %d\n", est_size );
		exit( 2 );
	}

	return pb;
}

/*
	Push a buffer on with realloc if needed.
*/
extern void pb_push( Push_buf_t* pb, char* str ) {
	int remain;
	int	need;

	if( ! pb ) {
		return;
	}

	if( ! str  ) {
		return;
	}

	if(  (need = strlen( str )) <= 0 ) {
		return;
	}

	remain = pb->nalloc - pb->used;
	need = strlen( str );
	if( remain - 1 <= need ) {
		pb->nalloc += pb->exsize;
		pb->buf = realloc( pb->buf, pb->nalloc );
	}

	strncpy( &pb->buf[pb->used], str, need + 1 );
	pb->used += need;
}

#define FGFL_CENTER	0x01		// local flags
#define FGFL_BOLD	0x02

extern void FMfigure( void )
{
	char *buf;		// input parameter
	int fnum = -1;				// possible number from the command
	int	tok_len;
	char	*cp;
	char	*type = "Figure";
	int	*num_src;				/* pointer to source for table or figure number */
	char	fbuf[100];			/* format buffer */
	char	vbuf[100];			// build .dv command in if vname given
	char	wbuf[1024];			// work buffer
	char*	vname = NULL;		// var name to put fig number into
	//int		center = 0;			// if x=center given we will flip center on
	int		loc_flags = 0;
	Push_buf_t* pb = NULL;
	int		ftextsize = 8;		// text size for the font string


	tok_len = FMgetparm( &buf );	// get early to check for nosect command and bail before any real work
	if( strcmp( buf, "nosect" ) == 0 ) {
		fmFlagClear( FIG_SECT_NUM );
		return;
	}

	num_src = &fig;			/* default to using figure number */

	FMflush( );            /* end current working line */
	pb = mk_pb( 512 );

	curfont = strdup( ffont == NULL ? curfont : ffont );	// default to figure font if there, text font if not, f= overrides later
	ftextsize = textsize >8 ? textsize - 2 : textsize;		// use a bit smaller, if not at a decent minimum

	while( tok_len > 0 && ((cp = strchr( buf, '=' )) != NULL) )
	{
		switch( *buf )
		{
			case 'f':
				if( *(cp+1) ) {
					free( curfont );				// free what we created before
					curfont = strdup( cp+1 );		// dup user font so we always free something at end
				}
				break;

			case 'n':			// n[um]=n  or na[me]=name
				if( *(cp + 1) ) {
					fnum = atoi( cp+1 );
				}
				break;

			case 'p':					// p[refix]=<string>
				if( fig_prefix ) {
					free( fig_prefix );
				}
				fig_prefix = strdup( cp+1 );
				break;

			case 's':			// override text size
				if( *(cp + 1) ) {
					textsize = atoi( cp+1 );
				}
				break;

			case 't':			// set type (figure or table)
				if( *(cp+1) == 't' )
				{
					type = "Table";
					num_src = &table_number;		// refrence the correct table number if needed
				}
				break;

			case 'v':			// var-name
				if( *(cp + 1) ) {
					vname = strdup( cp+1 );
				}
				break;

			case 'x':
				if( strcmp( cp+1, "bold" ) == 0 ) {
					loc_flags |= FGFL_BOLD;
					break;
				}
				if( strcmp( cp+1, "center" ) == 0 ) {
					loc_flags |= FGFL_CENTER;
					break;
				}

				if( strcmp( cp+1, "reset" ) == 0 ) {
					table_number = 1;
					fig = 1;
					break;
				}
		}

		tok_len = FMgetparm( &buf );
	}

	snprintf( wbuf, sizeof( wbuf ) -1, ".pu .sf %s .st %dp ", curfont, ftextsize );
	pb_push( pb, wbuf );

	if( fnum < 0 )				// not set on the command line
	{
		fnum = *num_src;		// pull from proper spot and bump up
		(*num_src)++;
	}

	if( fmFlagIsSet( PARA_NUM ) && fmFlagIsSet( FIG_SECT_NUM ) ) {			// if numbering paragraphs, add it to the number
		snprintf( fbuf, sizeof( fbuf ), "%s %d-%d: ", type, pnum[0], fnum );
		if( vname ) {
			snprintf( vbuf, sizeof( vbuf ), ".dv %s %d-%d : ", vname, pnum[0], fnum );
		}
	} else {
		if( fig_prefix ) {
			snprintf( fbuf, sizeof( fbuf ), "%s %s%d: ", type, fig_prefix, fnum );   /* gen fig number */
		} else {
			snprintf( fbuf, sizeof( fbuf ), "%s %d: ", type, fnum );   /* gen fig number */
		}
		if( vname ) {
			snprintf( vbuf, sizeof( vbuf ), ".dv %s %d : ", vname, fnum );
		}
	}

	if( loc_flags & FGFL_CENTER ) {
		snprintf( wbuf, sizeof( wbuf ) -1, ".bc start " );
		pb_push( pb, wbuf );
	}

	if( loc_flags & FGFL_BOLD ) {				//  we assume that there is xxx-Bold where xxx is the default or user set font
		snprintf( wbuf, sizeof( wbuf ) -1, ".tf %s-Bold %dp %s : ", curfont, textsize, fbuf );
		pb_push( pb, wbuf );
	} else {
		pb_push( pb, fbuf );
	}

	while( tok_len > 0 )		// add tokens, first already in the buffer
	{
		pb_push( pb, " " );
		pb_push( pb, buf );
		tok_len = FMgetparm( &buf );
	}
	pb_push( pb, " " );

	if( loc_flags & FGFL_CENTER  ) {
		snprintf( wbuf, sizeof( wbuf ) -1, ".bc end " );
		pb_push( pb, wbuf );
	}
	if( vname ) {
		pb_push( pb, vbuf );
		free( vname );
	}

	pb_push( pb, " .sp .1 .po " );						// final pop to restore previous font etc.
	AFIpushtoken( fptr->file, pb->buf );		// push the whole kit and kabottle
}

