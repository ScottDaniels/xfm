/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
//#include <time.h>

#include "libst/symtab.h"
 #include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"
#include "libxfm/xfm_const.h"


// #include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"
#include "libxfm/fmproto.h"
#include "pfmproto.h"

/*
 ---------------------------------------------------------------------
	Abstract:	Handles parameters for the .sf command and sets the
				font internally.

	Date: 		10 Dec 2022 - broken out from fmcmd and extended to 
					support strikeout.
	Author:		E. Scott Daniels
				
	.sf [b[egin]=fixed-begin] [s[trike]=<colour>|off] [h[ilight]=<colour>|off] [l[en]=<value>] [w=<weight>] {font-name|-}

	s == strike out (off resets line weight to default)
	h == highlight	(off resets fixed len to "auto")
	l == fixed highlight length; if not given "auto" len is set to match text length
	L == fixed highlight length with the start at the column edge
	w == line weight when strike is set.

	If a fixed highlight length is not given then the highlighting
	is set to be the length of the characters written while highlight
	is "on." If the len is given then highlighting is done for that
	exact length. This allows for sell background colouring in tables.

	In order to NOT be a breaking change, the fontname must be last
	and will terminate parameter processing. Font name may be - if
	it is not to be changed (allowing only a string/hilight change).

 ---------------------------------------------------------------------
*/


/*
	Ensure that the struct is allocated.
*/
static void ensureShConfig( ) {
	if( shConfig == NULL ) {
		shConfig = malloc( sizeof( struct strike_info ) );
		if( shConfig == NULL ) {
			fprintf( stderr, "(%s @ %ld) abort: unable to allocate memory for strikeout info", 
					fptr->name,  AFIstat( fptr->file, AFI_OPS, NULL ) );
			exit( 1 );
		}

		shConfig->fixedLen = -1;
		shConfig->fixedStart = -1;
		shConfig->lWeight = 1;
		shConfig->colour = NULL;
		shConfig->flags = FOFL_NONE;
	}
}

void FMsf() {
	char*	ptr;
	char*	p;
	int		len;

	while( (len = FMgetparm( &ptr )) != 0 )     		/* if a parameter was entered */
	{
		if( (p = strchr( ptr, '=' )) == NULL ) {		// pick up font name and boogie
			if( !isalpha( *ptr ) ) {
				if( *ptr == '-' ) {			// using - keeps the current name
					FMfmt_add( );		/* add a format block to the list */
				} else {
					fprintf( stderr, "(%s @ %ld) invalid font name: %s\n", fptr->name,  AFIstat( fptr->file, AFI_OPS, NULL ), ptr );
				}
			} else {
				TRACE( 2, "setfont old=%s  new=%s\n", curfont, ptr );
				free( curfont );          
				curfont = strdup( ptr );
				FMfmt_add( );		/* add a format block to the list */
			}

			return;
		}

		p++;					// one past =
		switch( *ptr ) {
			case 'b':			// a hard beginning location rather than text edge
				ensureShConfig();
				shConfig->fixedStart = FMgetpts( p, 0 );
				break;

			case 'h':			// highlight enable/disable
				if( strcmp( p, "off" ) == 0 ) {
					if( shConfig != NULL ) {
						shConfig->fixedLen = -1;
						shConfig->flags = FOFL_NONE;
						TRACE( 2, "setfont turning highlight off\n" );
					}
				} else {
					ensureShConfig();
					shConfig->flags = FOFL_HILITE;
					if( shConfig->colour != NULL ) {
						free( shConfig->colour );
					}
					shConfig->colour = FMparsecolour( p );
					TRACE( 2, "setfont turning hilite on colour='%s'\n", shConfig->colour );
				}
				break;

			case 'l':			// set a fixed highlight length
				ensureShConfig();
				shConfig->fixedLen = FMgetpts( p, 0 );
				break;

			case 's':			// strike out enabled/disable
				if( strcmp( p, "off" ) == 0 ) {
					if( shConfig != NULL ) {
						shConfig->lWeight = 1;
						shConfig->flags = FOFL_NONE;
						TRACE( 2, "setfont turning strikeout off\n" );
					}
				} else {
					ensureShConfig();
					shConfig->flags = FOFL_STRIKE;
					if( shConfig->colour != NULL ) {
						free( shConfig->colour );
					}
					shConfig->colour = FMparsecolour( p );
					TRACE( 2, "setfont turning strikeout on colour='%s'\n", shConfig->colour );
				}
				break;

			case 'w':			// strike weight
				ensureShConfig();
				shConfig->lWeight = atoi( p );
				TRACE( 2, "setfont line width = %d\n", shConfig->lWeight );
				break;

		}
	}
}
