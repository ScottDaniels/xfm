/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2024 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

#include <stdio.h>     
#include <stdlib.h>
#include <string.h> 

#include "libst/symtab.h"				// our utilities/tools
#include "libafi/afidefs.h"   

#include "libxfm/fmconst.h"
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"
#include "libxfm/fmproto.h"
#include "pfmproto.h"

/* --------------------------------------------------------------------------
*
* Mnemonic:	fmcolourpage
* Abstract: Called to process the PB command which is used to set the page
			background colour from the rgb string set as the only expected
			token
* Date:		25 July 2024
* Author:	E. Scott Daniels
* --------------------------------------------------------------------------
*/

/*
	Expect buf to be #rrggbb or 0xrrggbb. Convert to r g b string and set the
	variables pcR pcG and pcB variables in the postScript. We also set the
	flag to trigger newcp on page eject. If the buffer contains the string
	"off" then page background colour is turned off.
*/
extern void FMpage_colour( char* buf ) {
	char*	colourTokens;
	char*	tok;		// at token
	char*	bTok;		// at blank ktoken
	char	outStr[1024];

	if( strncmp( buf, "off", 3 ) == 0 ) {
		TRACE( 1, "turn page colour off\n" );
		fmFlagClear( COLOUR_PAGE_BG );
		return;
	}
	
	colourTokens = FMparsecolour( buf );
	TRACE( 1, "colour tokens: %s from buf: (%s)\n", colourTokens, buf );

	tok = colourTokens;
	bTok = strchr( tok, ' ' );
	*bTok = 0;
	TRACE( 2, "colour tokens: formatting %s from buf: (%s)\n", tok, buf );
	snprintf( outStr, sizeof( char ) * sizeof( outStr ), "/pcR %s def ", tok );
	AFIwrite( ofile, outStr );

	tok = bTok + 1;
	bTok = strchr( tok, ' ' );
	*bTok = 0;
	snprintf( outStr, sizeof( char ) * sizeof( outStr ), "/pcG %s def ", tok );
	AFIwrite( ofile, outStr );
	
	tok = bTok + 1;
	bTok = strchr( tok, ' ' );
	*bTok = 0;
	snprintf( outStr, sizeof( char ) * sizeof( outStr ), "/pcB %s def ", tok );
	AFIwrite( ofile, outStr );

	AFIwrite( ofile, "colourpage\n" );
	
	free( colourTokens );
	
	fmFlagSet( COLOUR_PAGE_BG );
}
