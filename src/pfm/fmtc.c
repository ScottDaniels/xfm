/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
	  		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
	  		of conditions and the following disclaimer in the documentation and/or other materials
	  		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/

#include <stdio.h>	 
#include <stdlib.h>
#include <fcntl.h>	
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>
#include <errno.h>

#include "libst/symtab.h"		// our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"			   // constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"			  // structure definitions */
#include "libxfm/fmproto.h"
#include "pfmproto.h"

/*
****************************************************************************
*
*   Mnemonic:  FMtc
*   Abstract:  This routine parses the .tc command and opens the toc file
*			  if it is not open already. As PSFM processes .h_ commands that
*			  cause TOC entries to be generated have PSFM source commands
*			  commands placed into the TOC file. When the last user source
*			  file is completed the TOC file is read by PSFM and the actual
*			  TOC is gerated. Thus PSFM generates PSFM code as is runs that
*			  it will also interpret prior to its termination!
*   Parms:	 None.
*   Returns:   Nothing.
*   Date:	  21 December 1988
*   Author:	E. Scott Daniels
*
*	Use:		tc {on [p] [i=indent] [w=col-width]|off} 
*					p -- page eject before starting the contents
*
*   Modified:  23 Apr 1991 - To open toc file based on buffer not const
*			  15 Dec 1992 - To convert to postscript and AFI
*			   7 Apr 1994 - To setup for TOC now that linesize/cd are points
*			17 Jul 2016 - Bring decls into the modern world.
*			13 Aug 2017 - remove code that turns off page numbering in toc.
***************************************************************************
*/
extern void FMtc( void ) {
	char	label[1024];				// label e.g. Table of Contents
	char *buf;							// pointer at the token */
	int len;							// len of parameter entered */
	int enable = 1;						// default to turning it on
	int page = 0;
	int ival = 54;						// defaults: indent .75i
	int wval = 540;						// width of 7.5i
	char wbuf[255];
	char* width = strdup( "7.5i" );
	char* indent = strdup( ".75i" );
	int label_snarf = 0;
	int remain;
	char*	tok;

	snprintf( label, sizeof( label )-1, "Table of Contents" );							// default label

	 while( (len = FMgetparm( &buf )) > 0 ) { 		// for all parms on the line
		if( label_snarf ) {
			if( (remain = sizeof( label ) - strlen( label ) -2) > strlen( buf ) ) { 		// shouldn't be arger than buf, but prevent accidents
				strcat( label, buf );
				strcat( label, " " );
				if( (tok = strchr( label, '"' )) != NULL ) {
					*tok = 0;
					label_snarf = 0;
				}
			}

			continue;
		}

		switch( *buf ) {
			case 'o':
				switch( buf[1] ) {			   // see what the user entered */
					case 'n':
					case 'N':				   // user wants it on */
					enable = 1;
					break;
		
					default:
						enable = 0;	
						break;
				}
				break;

			case 'i':						// assume i=indent
				indent = strdup( &buf[2] );
				ival = FMgetpts( indent, len-2 );
				break;

			case 'l':						// l=word or l="word word... word"
				if( buf[2] == '"' ) {
					snprintf( label, sizeof( label ), "%s", &buf[3] );
					if( (tok = strchr( label, '"' )) != NULL ) {
						*tok = 0;
						label_snarf = 0;
					} else {
						label_snarf = 1;
					}
				} else {
					snprintf( label, sizeof( label ), "%s", &buf[2] );
				}
				break;

			case 'p':						// page eject before
				page = 1;
				break;

			case 'w':						// assume w=col-width
				width = strdup( &buf[2] );
				wval = FMgetpts( width, len-2 );
				break;

			 default:
				break;
		}
  	}

	if( enable  ) {
		fmFlagSet( TOC );
	} else {
		fmFlagClear( TOC );
	}

	if( fmFlagIsSet( TOC )  &&  (tocfile < VALID) ) {		// need to open and setup file */
		tocfile = AFIopen( tocname, "w" );				// open file for writing */
		if( tocfile < 0 ) {
			FMmsg( E_CANTOPEN, tocname );
			return;
		}
							   // setup initial psfm commands in toc file */
		if( page ) {
			AFIwrite( tocfile, ".pa\n" );
		}
	
		snprintf( wbuf, sizeof( wbuf ), ".cd 1 %s i=%s : .st 15 .sf Times-Bold\n", width, indent );
		AFIwrite( tocfile, wbuf );
	
		snprintf( wbuf, sizeof( wbuf ), ".rh : .rf : .ju on .ll %dp .tc off\n", wval - (2 * ival ) );
		AFIwrite( tocfile, wbuf );

		AFIwrite( tocfile, ".bc start\n" );
		AFIwrite( tocfile, label );
		AFIwrite( tocfile, "\n.bc end\n" );

		snprintf( wbuf, sizeof( wbuf ), ".sp 2\n.ta %dp 1i V=-2,-3 s=0\n", wval - (2 * ival ) - 72 );		// set table with 1i right col and dynamic left col
		AFIwrite( tocfile, wbuf );
		TRACE( 2, "TOC file is now open\n" );
  	}

	free( width );
	free( indent );
}
