/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2022 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"

/*
*************************************************************************
*
*   Mnemonic: FMheader
*   Abstract: This routine is responsible for putting a paragraph header
*             into the output buffer based on the information in the
*             header options buffer that is passed in.
*   Parms:    hptr - Pointer to header information we will use
*   Returns:  Nothing.
*   Date:     1 December 1988
*   Author:   E. Scott Daniels
*
*   Modified: 30 Jun 1994 - Conversion to rfm (not much changed)
*             15 Dec 1994 - To seperate skip before/after numbers.
*             21 Mar 2001 - Remoiving the mothballs to mod to TFM
*		      06 Nov 2001 - Added spacing before/after 
*				17 Jul 2016 - Bring prototypes into modern era.
*				03 May 2020 - Fix before/after skip
*				20 Oct 2022 - Gen header label using libxfm function
*				7 Jan 2024 - convert to new flags format
**************************************************************************
*/
extern void FMheader( struct header_blk* hptr ) {
	int len;            /* len of parm token */
	int i;
	char buf[100];      /* buffer to build paragraph number in */
	char *tbuf;         /* pointer at temporary buffer to hold out buf string */
	char *ptr;          /* pointer to header string */
	int oldlmar;        /* temp storage for left mar value */
	int oldsize;        /* temp storage for text size value */
	int oldlen;         /* temp storage for old line length */
	char *oldfont;      /* pointer to old font sring */
	int  skip;          /* number of lines to skip before continuing */
	char* anno_buf = NULL;		// annotation buffer
	char	stxt[1024];			// full section  text for .gv

 	FMflush( );          /* flush the current line */

	memset( buf, 0, sizeof( buf ) );
	
	for( skip = hptr->bskip; skip > 0; skip-- ) {			// blank lines before
		AFIwrite( ofile, "\n" );
	}

	pnum[(hptr->level)-1] += 1;      /* increase the paragraph number */
	for( i = hptr->level; i < MAX_HLEVELS; i++ ) {
		pnum[i] = 0;                       /* zero out all lower p numbers */
	}

	if( fmFlagIsSet( PARA_NUM ) ) {
		if( hptr->level == 1 ) {
			fig = 1;					//restart figure numbers if top level header
		}

		tbuf = FMmk_header_snum( hptr->level );	// format the header label
		snprintf( obuf, IO_BUF_LEN, "%s ", tbuf );
		optr = strlen( obuf );
	}

	stxt[0] = 0;
	while( (len = FMgetparm( &ptr )) > 0 ) {    /* get next token in string */
		for( i = 0; i < len; i++, optr++ ) {
			if( hptr->flags & HTOUPPER ) {		// xlate to upper flag for this header
				obuf[optr] = toupper( ptr[i] );
			} else {
				obuf[optr] = ptr[i];			// copy in the buffer
			}
		}
		
		if( (strlen( stxt ) + len +1) < sizeof( stxt ) - 1 ) {
			if( stxt[0] ) {
				strcat( stxt, " " );
			}
			strcat( stxt, ptr );
		}

		obuf[optr++] = BLANK;            /* add an additional blank */
	}

	FMmk_header_stxt( stxt );			// make header text available for .gv sect command

	obuf[optr-1] = EOS;				// terminate the header string; chopping last blank
	tbuf = strdup( obuf );           /* hold the header stuff for a bit */
	optr = strlen( tbuf );

	oldlmar = lmar;                  /* save left margin */
	lmar = hptr->hmoffset;

	if( hptr->atype != ANNO_NONE ) {				// set up for annotation
		len = strlen( obuf );
		anno_buf = (char *) malloc( sizeof( char ) * (len + 1) );
		memset( anno_buf, hptr->ach, len );
		anno_buf[len] = 0;
		if( hptr->atype != ANNO_AFTER ) { 			// before or both, write above
			strcpy( obuf, anno_buf );
			FMflush( );	
			strcpy( obuf, tbuf );
			optr = strlen( tbuf );
		}
	}

 	FMflush( );                      				// output the header text

	if( hptr->atype == ANNO_AFTER || hptr->atype == ANNO_BOTH) { 			// need to write annotaton after
		strcpy( obuf, anno_buf );
	 	optr = strlen( tbuf );
		FMflush( );	
	}
	if( anno_buf ) {
		free( anno_buf );
	}

 	for( skip = hptr->askip; skip > 0; skip-- ) {
		AFIwrite( ofile, "\n" );
	}

	strcpy( obuf, tbuf );            /* restore header stuff */
	if( tbuf ) {
		free( tbuf );
	}

	/* the call to FMtoc must be made while the header is in obuf */
	if( hptr->flags & HTOC ) {         /* put this in table of contents? */
		FMtoc( hptr->level );           /* put entry in the toc file */
	}

	oldlen = linelen;                /* save the line length */
	oldfont = curfont;               /* save current text font */
	oldsize = textsize;              /* save old text size */

	textsize = hptr->size / 4;       /* set text size to header size for flush */
	curfont = hptr->font;            /* set font to header font for flush */

	FMflush( );                      /* writeout the header line */

 	linelen = oldlen;                /* restore line length */
 	lmar = oldlmar;                  /* restore left margin value */
 	textsize = oldsize;              /* restore text size */
 	curfont = oldfont;               /* restore font too */

	fmFlagSet( SETFONT );		// header font requiers change maybe

	for( optr = 0, i = 0; i < hptr->indent; i++, optr++ ) {   /* indent */
		obuf[optr] = BLANK;
	}

	obuf[optr] = EOS;                          /* terminate incase flush called */
	osize = hptr->indent;                      /* set size of blanks */
}                    /* FMheader */
