/*
All source and documentation in the xfm tree are published with the following open source license:
Contributions to this source repository are assumed published with the same license. 

=================================================================================================
	(c) Copyright 1995-2015 By E. Scott Daniels. All rights reserved.

	Redistribution and use in source and binary forms, with or without modification, are
	permitted provided that the following conditions are met:
	
   		1. Redistributions of source code must retain the above copyright notice, this list of
      		conditions and the following disclaimer.
		
   		2. Redistributions in binary form must reproduce the above copyright notice, this list
      		of conditions and the following disclaimer in the documentation and/or other materials
      		provided with the distribution.
	
	THIS SOFTWARE IS PROVIDED BY E. Scott Daniels ``AS IS'' AND ANY EXPRESS OR IMPLIED
	WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
	FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL E. Scott Daniels OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
	CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
	SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
	ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
	ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
	
	The views and conclusions contained in the software and documentation are those of the
	authors and should not be interpreted as representing official policies, either expressed
	or implied, of E. Scott Daniels.
=================================================================================================
*/


#include <stdio.h>     
#include <stdlib.h>
#include <fcntl.h>    
#include <ctype.h>   
#include <string.h> 
#include <memory.h>
#include <time.h>

#include "libst/symtab.h"		/* our utilities/tools */
#include "libafi/afidefs.h"   


#include "libxfm/fmconst.h"               /* constant definitons */
#include "libxfm/xfm_const.h"


#include "libxfm/fmcmds.h"
#include "libxfm/fmstruct.h"              /* structure definitions */
#include "libxfm/fmproto.h"

/*
****************************************************************************
* TFM
*
*   Mnemonic: FMflush
*   Abstract: This routine is responsible for flushing the output buffer
*             to the output file and resetting the output buffer pointer.
*				The return value is to be consistent with other versions and
*				is meaningless in the context of tfm.
*   Parms:    None.
*   Date:     15 November 1988
*   Author:   E. Scott Daniels
*
*   Modified: 29 Jun 1994 - To convert to rfm.
*              6 Dec 1994 - To reset font even if nothing to write.
*              7 Dec 1994 - To prevent par mark at page top if in li list
*             14 Dec 1994 - To prevent par mark at top if in di list
*              6 Dec 1996 - To convert for hfm
*              9 Nov 1998 - To remove </font> - Netscape seems unable to
*	                    handle them properly
*             22 Jan 2000 - To strncmp for color rather than strcmp
*             21 Mar 2001 - Revisiting an old friend; tfm conversion
*			07 Jul 2013 - Support centered text
*				17 Jul 2016 - Bring prototypes into modern era.
*				11 Jul 2020 - major revision to (finally) fix indent issue in
*								list items when .in 0 is specified.
*				7 Jan 2024 - convert to new flags format
****************************************************************************
*/
extern int FMflush( void )
{
	int len;                /* length of the output string */
	char fbuf[2048];        /* buffer to build flush strings in */
	int iv = 0;				// text indention value
	int ip = 0;				// insertion point
	int	term_space;			// amount reserved for current def list
	int	term_len;			// if diterm -- its length
	int extra;

	*fbuf = 0;
	iv = (lmar/7) + 1;		// insertion point for the output modulo dlist or list additions
	if( iv < 1 ) {			// maintain backward compatability which always adde a lead space
		iv = 1;
	}

	if( lilist && lilist->xpos == 0 ) {		// list item
		memset( fbuf, ' ', iv + 2 );
		if( iv < 2 ) {
			iv = 2;
		}
		lilist->xpos = iv;

		fbuf[iv-2] = lilist->ch;;
		fbuf[iv] = 0;
	} else {
		if( fmFlagIsSet( DIBUFFER ) ) {      // need to back off some if term in buffer
			term_space = dlstack[dlstackp].indent/7;			// 	amount reserved for the term

			if( term_space > sizeof( fbuf ) ) {
				fprintf( stderr, "abort:  diterm too long for buffer\n" );
				exit( 1 );
			}

			term_len = strlen( di_term );								// actual string length
			memset( fbuf, ' ', iv + 1 );								// clear all in case term is short
			fbuf[iv+2] = 0;

			ip = iv - term_space;											// compute insertion point
			if( ip < 0 ) {
				ip = 0;
			}
			if( sizeof( fbuf ) -1 > ip + term_len ) {
				memcpy( fbuf+ip, di_term, term_len );
				if( term_len + ip > iv ) {									// long term length allowed; caller will not be writing def on same line
					fbuf[ip+term_len] = 0;
					iv +=  strlen( fbuf );	
				} else {
					fbuf[iv] = 0;
				}
			} else {
				fprintf( stderr, "### WARN ### term length would cause buffer overrun\n" );
				*fbuf = 0;
			}
		} else {
			if( fmFlagIsSet( CENTER ) ) {
				len = strlen( obuf );
				memset( fbuf, ' ', iv + (len/2) );						// ensure leading spaces for as much as we might need
				fbuf[(iv + (len/2))+1] = 0;
				if( (extra = ((linelen/7)-iv) - len ) > 2 ) {
					iv += extra/2;
				}
			} else {
				memset( fbuf, ' ', iv );
				fbuf[iv+1] = 0;
			}
		}
	}

	TRACE( 2, "flush: lmar=%d obuf=(%s) iv=%d optr=%d\n", lmar,  obuf, iv, optr );
	fmFlagClear( DIBUFFER );

	if( optr == 0 ) { 		 /* nothing to do so return */
		FMreset_ti();
		return 0;
	}

	if( !fmFlagIsSet( CENTER ) ) {
		FMjustify( );                          /* justify if on, and not centering */
	}

	memcpy( &fbuf[iv], obuf, strlen( obuf ) + 1 );		// smash on obuf to li/di stuff if there
	AFIwrite( ofile, fbuf );

	if( fmFlagIsSet( DOUBLESPACE ) ) {
		strcpy( fbuf, " " );
		AFIwrite( ofile, fbuf );
	}
	
	*obuf = EOS;				// reset things
	optr = 0;

	FMreset_ti();
	return 0;
}
